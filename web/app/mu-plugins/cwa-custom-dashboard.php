<?php
/*
Plugin Name: Comrade Web Agency Dashboard
Description: Primary function.
Author: CWA
Version: 1
Author URI: https://comradeweb.com
*/

function custom_dashboard_widget(){
    echo '<link rel="stylesheet" type="text/css" href="'.plugins_url( 'dw_style.css', __FILE__ ).'" />';
    if(is_rtl()){
        echo '<style>#dashboard-widgets .dashicons{  margin-right: 0; margin-left: 40px; }</style>';
    }
    $icon_array = [
        'posts' => [
            'icon' => 'dashicons-admin-post',
            'link' => '/wp/wp-admin/edit.php',
            'name' => 'Posts'
        ],
        'media' => [
            'icon' => 'dashicons-admin-media',
            'link' => '/wp/wp-admin/upload.php',
            'name' => 'Media'
        ],
        'page' => [
            'icon' => 'dashicons-admin-page',
            'link' => '/wp/wp-admin/edit.php?post_type=page',
            'name' => 'Pages'
        ],
        'flamingo' => [
            'icon' => 'dashicons-feedback',
            'link' => '/wp/wp-admin/admin.php?page=flamingo',
            'name' => 'Contact Form DB'
        ],
        'contact' => [
            'icon' => 'dashicons-buddicons-pm',
            'link' => '/wp/wp-admin/admin.php?page=wpcf7',
            'name' => 'Contact Forms'
        ],
        'settings' => [
            'icon' => 'dashicons-admin-settings',
            'link' => '/wp/wp-admin/admin.php?page=theme-general-settings',
            'name' => 'Theme Settings'
        ]
    ];
    foreach ($icon_array as $value) {
            echo '<div class="main_bashboard_widget_button">
					<a href="'.$value['link'].'">
						<span class="dashicons '.$value['icon'].'"></span>
						<h3>'.$value['name'].'</h3>
					</a>
        </div>';
    }
}
function add_custom_dashboard_widget(){
    wp_add_dashboard_widget('custom_dashboard_widget',__('Dashboard', 'DashboardWidgets'),'custom_dashboard_widget','rc_mdm_configure_my_rss_box');
}
add_action('wp_dashboard_setup', 'add_custom_dashboard_widget');