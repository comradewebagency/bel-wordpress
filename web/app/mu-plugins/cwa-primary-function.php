<?php
/*
Plugin Name: Comrade Web Agency Settings
Description: Primary function.
Author: CWA
Version: 1
Author URI: https://comradeweb.com
*/

//Phone Number
function number_phone($number)
{
    $sPhone = $number;
    $phone = trim($sPhone);
    $phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);
    $OriginalPhone = '+1' . $phone;
    return $OriginalPhone;
}

//Remove span disable
function pp_override_mce_options($initArray)
{
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}

add_filter('tiny_mce_before_init', 'pp_override_mce_options');

remove_filter('wpcf7_validate_radio', 'wpcf7_checkbox_validation_filter', 10);

function link_primary($class, $link_array, $rel = null, $wrapper_start = null, $wrapper_end = null)
{
    if ($link_array) {
        $link_attribute = '';
        if ($class) {
            $link_attribute .= ' class="' . $class . '" ';
        }
        if ($link_array['url']) {
            $link_attribute .= ' href="' . $link_array['url'] . '" ';
        }
        if ($link_array['target']) {
            $link_attribute .= ' target="' . $link_array['target'] . '" ';
        }
        if ($rel) {
            $link_attribute .= ' rel="' . $rel . '" ';
        }
        if ($wrapper_start && $wrapper_end) {
            $link_array['title'] = $wrapper_start . $link_array['title'] . $wrapper_end;
        }
        return '<a ' . $link_attribute . '>' . $link_array['title'] . '</a>';
    }
}

add_filter('wpcf7_autop_or_not', '__return_false');

//Custom Alt
function custom_alt($origin_alt, $custom_alt)
{
    if ($origin_alt) {
        $alt = $origin_alt;
    } else {
        $alt = $custom_alt;
    }
    return $alt;
}

function wp_get_menu_array($current_menu)
{

    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    $submenu = array();

    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $curent_id = $m->ID;
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID'] = $m->ID;
            $menu[$m->ID]['title'] = $m->title;
            $menu[$m->ID]['url'] = $m->url;
            $menu[$m->ID]['submenu'] = array();
        }

        if ($m->menu_item_parent == $curent_id) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID'] = $m->ID;
            $submenu[$m->ID]['title'] = $m->title;
            $submenu[$m->ID]['url'] = $m->url;
            $menu[$m->menu_item_parent]['submenu'][$m->ID] = $submenu[$m->ID];
        }

    }

    return $menu;
}


function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

//Link Auto Blank
function autoblank($text)
{
    $uri = $_SERVER['REQUEST_URI'];
    $urip = $_SERVER['HTTP_HOST'];
    $ur = "http://" . $urip . $uri;
    $return = str_replace('href=', 'target="_blank" href=', $text);
    $return = str_replace('target="_blank" href="' . $ur . '', 'href="' . $ur . '', $return);
    $return = str_replace('target="_blank" href="/', 'href="/', $return);
    $return = str_replace('target="_blank" href="#', 'href="#', $return);
    $return = str_replace(' target = "_blank">', '>', $return);
    return $return;
}

add_filter('the_content', 'autoblank');
add_filter('comment_text', 'autoblank');

//Theme Menu Registration
add_action('after_setup_theme', function () {
    register_nav_menus(array(
        'header_menu' => 'Header Menu',
        'footer_menu' => 'Footer Menu',
    ));
});

function my_acf_google_map_api($api)
{
    $api['key'] = get_field('field_settings_google_map_key', 'option');
    return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function picture($images, $alt = 'default', $default = 'full', $class = 'lozad',  $default_src = null)
{
    if (is_array($images)) {
        $image = $images['id'];
        if ($alt == 'default' || !$alt) {
            $alt = $images['alt'];
        }
    } else {
        $image = $images;
        if ($alt == 'default' || !$alt) {
            $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);;
        }
    }
    $default_src = empty($default_src) ? get_template_directory_uri() . '/front/dist/assets/img/load.svg' : $default_src;

    $data_src = wp_get_attachment_image_url($image, $default);
    $data_srcset = wp_get_attachment_image_srcset($image, $default);
    $sizes = wp_get_attachment_image_sizes($image, $default);

    $html = "<img src='{$default_src}'";

    if($alt != 'default') {
        $html .= " alt='{$alt}'";
    }

    if($class) {
        $html .= " class='{$class}'";
    }

    if($data_src) {
        $html .= " data-src='{$data_src}'";
    }

    if($data_srcset) {
        $html .= " data-srcset='{$data_srcset}'";
    }

    if($sizes) {
        $html .= " sizes='{$sizes}'";
    }

    $html .= '/>';


    $key = 'image_'.$image;
    $result = wp_cache_get( $key );

    if ( false === $result ) {
        $result = $html;
        wp_cache_set( $key, $result );
    }

    return $result;
}

if (!function_exists('disable_gutenberg')) {
    function disable_gutenberg()
    {

        global $wp_filter;

        $callbacks_array = $wp_filter['init']->callbacks;

        foreach ($wp_filter as $tag => $priorities) {
            foreach ($priorities->callbacks as $priority => $callback_data) {
                foreach ($callback_data as $callback_function_name => $callback_function_data) {

                    if (strpos($callback_function_name, 'disable_gutenberg') !== false) {
                        continue;
                    }

                    // Gutenberg disabler
                    if (strpos($callback_function_name, 'gutenberg') !== false || strpos($callback_function_name, 'block_editor')) {

                        remove_filter($tag, $callback_function_name, $priority);

                    }

                }
            }
        }

        $wp_filter['init']->callbacks = $callbacks_array;

        add_filter('use_block_editor_for_post_type', '__return_false');
    }
}
add_action('admin_init', 'disable_gutenberg');
