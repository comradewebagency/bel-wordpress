<?php
/*
Plugin Name: Comrade Web Agency Admin Panel
Description: Primary function.
Author: CWA
Version: 1
Author URI: https://comradeweb.com
*/
## Add ComradeWebLogo in admin bar
add_action('add_admin_bar_menus', 'reset_admin_wp_logo');
function reset_admin_wp_logo() {
    remove_action('admin_bar_menu', 'wp_admin_bar_wp_menu');
    add_action('admin_bar_menu', 'add_admin_bar_logo');
}

function add_admin_bar_logo( WP_Admin_Bar $wp_admin_bar ) {
    $wp_admin_bar->add_menu([
        'id' => 'wp-logo',
        'title' => '<img style="height: 32px; width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAuCAYAAABTTPsKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjc3NzJEOUFEQTY1QTExRTlCNUQ5QzI1M0VFNDg2OTc2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjc3NzJEOUFFQTY1QTExRTlCNUQ5QzI1M0VFNDg2OTc2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Nzc3MkQ5QUJBNjVBMTFFOUI1RDlDMjUzRUU0ODY5NzYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Nzc3MkQ5QUNBNjVBMTFFOUI1RDlDMjUzRUU0ODY5NzYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5LPg8sAAAETElEQVR42tRZTUhUURSeeQlhJlG0yf42lWiF1UJBoSRIsqgxkGpTKCGUKzU0s6Q2SY5g0abAfiSDdCE6BSFCYkkSbgoqzUVtCoMWSZlJYdr3DffB6/Vm3nkz743ThcO5M+/de7977jnnnnOef2N1sc+tNhp8sAhsGSjN9Gga9DW77uDveNdIiRfg/Pz8JnR3gfLQXwe+Gjzd7/enGl79TMBvmkOjHAZ6omnaq6zaAzNO1/THImEAXQtQR9EtBc+NZbPY0DhYJ6hr85nAmCeAFdBqdMvAl7ukST8B/j54UAJcDBjHeRKsyUWgVsAbQa3RdN0WMKS6Ym5u7ja6AV8CGgCPgEoB+oPVc02gAsOJAstGm4CAnuNEcx0BxoAsDsQEmb7EtwxQnxVoLZJkwXrUwAVptBVQDwUXVYfpWyHZ7hjUgEbzDHwI9A70zfBsOxbfhuc7nRotdRo+u1D32SkWO6txAhYTToK1g1+JZChoId2AwU5gjSrp6SmdvoRuzT8qocR/1gHYAbAC+E9ONmH3Pjb0Be+2QGJbMbbdgXpU6vps1uE66ZFRoqAi3dk7iRMU8HKMrxMOWQxcDX/pMKWLP1/woQBsOxd0w7h4IWHd60Ih5RklXCEESyOodMsbYOM3HKjHqTDgsZaHjKz2CQc1xBJl2QjhtMQGcBJFYcCwwnzhBRGCRB677XOp0wB91cZlDvAd3a3tEErijocxxC2ju6O7xO+n4N2gQd1lakrUBYI5JzBwyCvASspdPEXwEtCGLfUlJTjRDqO6+GdnZ1OhEoOCQDzECby8jnnL2rlHDUBTQWsER/be6/hB4supEmmm/CtS++RLgqY5eHfqfwOcniyAp6HDkotgVVIAhv7OgD4K3s1JBsDh4Of15d5euxiYjhyUEyXmdSMQugi2HtRLn0/fbFn5wcOXUIuAXcoCVgjq8MgHMzuvUDcd6x4T2EA/+gPGm06/mqU3WJlXgAHwsCkLycB/ZWrNSWgBr+m+sJdAuDisSkd2k+7GrgNeSBesyuZ0A+CNYcAqXHwknL9ZLeCmdM9LokUItV/T73CwNoZxgskzWQlSY9wwtGOqXidp9zT9DmdupopyksbjuRkvaAW2TZruMxY333RBlbZLjpGW3G8udEgaMxyMa8X4u5K0TM90LAspmKgWEwUd1iWuMQC389HUfcy9H91zTkpgxqQ3xeJhqwroA0JJ04IvgNdjs5EqP6zSZ0P3i5yWv+i9VM5nXaoySGJ4gQqB5tPbC+mORI3W1JV4SOKbPWz0WOVGsFHDS+U19tA6F0KyoCPAEHIUD9OIMLDYSR3MBbDjql4XiimAN9TBjkuKHfGogKrX5Uf7OJMMX5F8KrVvMutr3IBNwN34Tsf4pVMCNC7AxjqC8UsoNYglA2bh4EsUsB9MwcCnwN8y9qavZoSYsC+h0YofvHYBbCVoqQL83Zcs35qtih8A+svLNOqPAAMA8vBNTXvOj0gAAAAASUVORK5CYII=">',
        'href' => '#'
    ]);
}

## Removing Widgets from the WordPress Console
add_action( 'wp_dashboard_setup', 'clear_dash', 99 );
function clear_dash(){
    $side   = & $GLOBALS['wp_meta_boxes']['dashboard']['side']['core'];
    $normal = & $GLOBALS['wp_meta_boxes']['dashboard']['normal']['core'];
    $remove = array(
        'dashboard_activity',
        'dashboard_primary',
        'dashboard_right_now',
        'dashboard_quick_press',
    );
    foreach( $remove as $id ){
        unset( $side[$id], $normal[$id] );
    }
    remove_action( 'welcome_panel', 'wp_welcome_panel' );
}

//Wordpress version clean
function true_remove_wp_version_wp_head_feed() {
    return '';
}
add_filter('the_generator', 'true_remove_wp_version_wp_head_feed');

//Redirect Attacment
add_action('template_redirect', 'template_redirect_attachment');
function template_redirect_attachment() {
    global $post;
    if (is_attachment()) {
        wp_redirect(get_permalink($post->post_parent));
    }
}

function sar_attachment_redirect() {
    global $post;
    if (is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0) ) {
        wp_redirect(get_permalink($post->post_parent), 301);
        exit;
    } elseif (is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1) ) {
        wp_redirect(get_bloginfo('wpurl'), 302);
        exit;
    }
}

function sar_archive_redirect()
{
    global $post;
    if (is_date())
    {
        wp_redirect(home_url(), 301);
        exit;
    }
}
add_action('template_redirect', 'sar_attachment_redirect' ,1);
add_action('template_redirect', 'sar_archive_redirect' ,1);

//Footer link
function true_change_admin_footer () {
    $footer_text = array(
        'Developed by ComradeWebAgency <a target="_blank" href="https://comradeweb.com">ComradeWebAgency</a>'
    );
    return implode( ' &bull; ', $footer_text);
}
add_filter('admin_footer_text', 'true_change_admin_footer', 99999);

add_action( 'wp_before_admin_bar_render', 'wpse200296_before_admin_bar_render' );
function wpse200296_before_admin_bar_render()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('customize');
}

//Disable gutenberg style in Front
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

//Support
add_editor_style();
add_theme_support('editor-styles');
add_theme_support('automatic-feed-links');
add_theme_support('responsive-embeds');
add_theme_support('menus');
add_theme_support('soil-clean-up');
//add_theme_support('soil-disable-rest-api');
add_theme_support('soil-disable-asset-versioning');
add_theme_support('soil-disable-trackbacks');
add_theme_support('soil-js-to-footer');
add_theme_support('soil-relative-urls');
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );