<?php
function cwa_scripts_styles(){
    wp_deregister_script( 'jquery' );
    wp_deregister_script( 'wp-embed' );
    wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, null, true);
    wp_enqueue_script('jquery');

    wp_enqueue_script( 'runtime', get_template_directory_uri() . '/front/dist/assets/js/runtime.js', array(), false, true );

    if ( is_page_template('pages/index.php')) {
        wp_enqueue_style('home-css', get_template_directory_uri() . '/front/dist/assets/css/home.styles.css');
        wp_register_script('home-js', get_template_directory_uri() . '/front/dist/assets/js/home.js', array('runtime'), null, true);
        wp_enqueue_script('home-js');
    }

    if (is_singular('practice')) {
        wp_enqueue_style('practice-inner-css', get_template_directory_uri() . '/front/dist/assets/css/practice-area-inner.styles.css');
        wp_register_script('practice-inner-js', get_template_directory_uri() . '/front/dist/assets/js/practice-area-inner.js', array('runtime'), null, true);
        wp_enqueue_script('practice-inner-js');
    }
    if (is_singular('perspectives')) {
        wp_enqueue_style('perspectives-css', get_template_directory_uri() . '/front/dist/assets/css/perspectives-inner.styles.css');
        wp_register_script('perspectives-js', get_template_directory_uri() . '/front/dist/assets/js/perspectives-inner.js', array('runtime'), null, true);
        wp_enqueue_script('perspectives-js');
    }
    if (is_singular('attorneys')) {
        wp_enqueue_style('attorneys-css', get_template_directory_uri() . '/front/dist/assets/css/attorneys-inner.styles.css');
        wp_register_script('attorneys-js', get_template_directory_uri() . '/front/dist/assets/js/attorneys-inner.js', array('runtime'), null, true);
        wp_enqueue_script('attorneys-js');
    }

    if (is_page_template('pages/practice-area.php')) {
        wp_enqueue_style('practice-area-css', get_template_directory_uri() . '/front/dist/assets/css/practice-area.styles.css');
        wp_register_script('practice-area-js', get_template_directory_uri() . '/front/dist/assets/js/practice-area.js', array('runtime'), null, true);
        wp_enqueue_script('practice-area-js');
    }

    if (is_page_template('pages/perspectives.php') || is_search()) {
        wp_enqueue_style('blog-css', get_template_directory_uri() . '/front/dist/assets/css/perspectives.styles.css');
        wp_register_script('blog-js', get_template_directory_uri() . '/front/dist/assets/js/perspectives.js', array('runtime'), null, true);
        wp_enqueue_script('blog-js');
    }

    if (is_page_template('pages/attorneys.php')) {
        wp_enqueue_style('attorneys-css', get_template_directory_uri() . '/front/dist/assets/css/Attorneys.styles.css');
        wp_register_script('attorneys-js', get_template_directory_uri() . '/front/dist/assets/js/Attorneys.js', array('runtime'), null, true);
        wp_enqueue_script('attorneys-js');
    }
    if (is_page_template('pages/advanteges.php')) {
        wp_enqueue_style('advanteges-css', get_template_directory_uri() . '/front/dist/assets/css/advanteges.styles.css');
        wp_register_script('advanteges-js', get_template_directory_uri() . '/front/dist/assets/js/advanteges.js', array('runtime'), null, true);
        wp_enqueue_script('advanteges-js');
    }
    if (is_page_template('pages/payment.php')) {
        wp_enqueue_style('payment-css', get_template_directory_uri() . '/front/dist/assets/css/advanteges.styles.css');
        wp_register_script('payment-js', get_template_directory_uri() . '/front/dist/assets/js/advanteges.js', array('runtime'), null, true);
        wp_enqueue_script('payment-js');
    }
    if (is_page_template('pages/careers.php')) {
        wp_enqueue_style('careers-css', get_template_directory_uri() . '/front/dist/assets/css/careers.styles.css');
        wp_register_script('careers-js', get_template_directory_uri() . '/front/dist/assets/js/careers.js', array('runtime'), null, true);
        wp_enqueue_script('careers-js');
    }

    if (is_page_template('pages/client-results.php')) {
        wp_enqueue_style('client-css', get_template_directory_uri() . '/front/dist/assets/css/client-result.styles.css');
        wp_register_script('client-js', get_template_directory_uri() . '/front/dist/assets/js/client-result.js', array('runtime'), null, true);
        wp_enqueue_script('client-js');
    }
    if (is_page_template('pages/contact-us.php')) {
        wp_enqueue_style('contact-us-css', get_template_directory_uri() . '/front/dist/assets/css/contact-us.styles.css');
        wp_register_script('contact-us-js', get_template_directory_uri() . '/front/dist/assets/js/contact-us.js', array('runtime'), null, true);
        wp_enqueue_script('contact-us-js');
    }
    if (is_page() && !is_page_template()) {
        wp_enqueue_style('template_css', get_template_directory_uri() . '/front/dist/assets/css/template.styles.css');
        wp_enqueue_script('template_js', get_template_directory_uri() . '/front/dist/assets/js/template.js', array(), false, true);
        wp_enqueue_script('template-js');
    }

    if (is_404()) {
        wp_enqueue_style('not-found_css', get_template_directory_uri() . '/front/dist/assets/css/not-found.styles.css');
        wp_enqueue_script('not-found_js', get_template_directory_uri() . '/front/dist/assets/js/not-found.js', array(), false, true);
        wp_enqueue_script('not-found-js');
    }

    wp_enqueue_script( 'loadmore', get_stylesheet_directory_uri() . '/js/loadmore.js', array('jquery'), null, true );
    wp_enqueue_script( 'loadmore2', get_stylesheet_directory_uri() . '/js/loadmore2.js', array('jquery'), null, true );
    wp_enqueue_script( 'loadmore3', get_stylesheet_directory_uri() . '/js/loadmore3.js', array('jquery'), null, true );
    wp_enqueue_script( 'loadmore4', get_stylesheet_directory_uri() . '/js/loadmore4.js', array('jquery'), null, true );

}
add_action( 'wp_enqueue_scripts', 'cwa_scripts_styles', 1 );

