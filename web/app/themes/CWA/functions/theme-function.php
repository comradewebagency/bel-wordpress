<?php
//Disable Comment
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );

// <div class="hero__pagination"><a class="hero__pagination-link" href="#">Home</a><span class="hero__pagination-delimiter"><img src="assets/img/arrow-pagination.svg" alt="img"></span><span class="hero__pagination-link">Practice Areas</span>
function the_breadcrumb() {
    $post_types = array('blog');
    $sep = '<span class="hero__pagination-delimiter"><img src='.get_template_directory_uri().'/front/dist/assets/img/arrow-pagination.svg ></span>';
    echo '<div class="hero__pagination-wrap  "><div class="container"><div class="hero__pagination">';
    if (!is_front_page()) {
        echo '<a style="cursor: pointer" class="hero__pagination-link" href="/';
        echo '">Belvedere Legal</a>';
        echo $sep;
        if(!is_singular("practice") && !is_singular("perspectives") && !is_singular('post') && !is_singular("attorneys") && !is_search() ) {
            echo ' <span class="hero__pagination-link"> '.get_the_title().'</span>';
        }
        if(is_single()) {
            $info = get_post_type_object(get_post_type());
            echo '<a class="hero__pagination-link" href="/'.$info->rewrite['slug'].'/">';
            echo $info->label.'</a>';
            echo $sep;
            echo ' <div class="hero__pagination-link" >' .the_title().'</div>';
        }
        if (is_search()) {
            echo '<span class="hero__pagination-link">Search Results</span>';
        }
    }
    else {
        echo 'Home';
    }
    echo '</div></div></div>';
}



