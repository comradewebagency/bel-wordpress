<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$blog = new FieldsBuilder('blog');
$blog
    ->addText('hero_title',['label' => 'Title'])
    ->addImage('hero_image',['label' => 'Image'])
    ->addText('link', ['label' => 'Button'])
    // ->addText('cases_title', ['label' => 'Perspectives Title'])

    ->setLocation('page_template', '==', 'pages/perspectives.php')
    ->setGroupConfig('hide_on_screen', [
        'the_content',
        'the_excerpt'
    ]);
add_action('acf/init', function () use ($blog) {
    acf_add_local_field_group($blog->build());
});
