<?php use StoutLogic\AcfBuilder\FieldsBuilder;

$attorneys = new FieldsBuilder('attorneys');
$attorneys
        ->addText('bio_text', ['label' => 'Button 1'])
        ->addText('bio_texts', ['label' => 'Button 2'])
    // ->addRepeater('testimonials_list', ['label' => 'Item', 'layout' => 'block'])
    //     ->addImage('testimonials_user', ['label' => 'Image'])
    //     ->addText('testimonials_title', ['label' => 'Title'])
    //     ->addText('testimonials_work', ['label' => 'Work'])
    //     ->addWysiwyg('testimonials_text', ['label' => 'Text'])
    //     ->addLink('testimonials_link', ['label' => 'Button'])
    //     ->addLink('testimonials_link2', ['label' => 'Button'])
    // ->endRepeater()
    ->setLocation('page_template', '==', 'pages/attorneys.php')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);
add_action('acf/init', function() use ($attorneys) {
    acf_add_local_field_group($attorneys->build());
});
