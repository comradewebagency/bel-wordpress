<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$cpt_attorneys = new FieldsBuilder('cpt_attorneys');
$cpt_attorneys
    ->addTab('Description')
        ->addText('title',['label' => 'Quote'])
        ->addText('testimonials_work', ['label' => 'Admission'])
        ->addText('link_phone',['label' => 'Phone'])
        ->addText('link_email',['label' => 'Email'])
        ->addImage('user_image',['label' => 'User Image'])
            ->setWidth('50')
        ->addImage('image',['label' => 'Background'])
            ->setWidth('50')
        ->addWysiwyg('desription',['label' => 'Desription'])
        ->addTrueFalse('more_on', ['label' => 'View Infoblock','ui' => 0])
        ->addRepeater('testimonials_list', ['label' => 'Item', 'layout' => 'block'])
            ->addText('biography_title',['label' => 'Title '])
                ->setWidth('50')
            ->addWysiwyg('biography_text',['label' => 'Content'])
                ->setWidth('50')
        ->endRepeater()
    ->addTab('How to Apply')
        ->addText('title_apply',['label' => 'Title'])
        ->addLink('link_apply',['label' => 'Button'])
        ->addWysiwyg('content_apply',['label' => 'Content'])
    
    // ->conditional('more_on', '==', '1')
    //     ->addTrueFalse('more_on', ['label' => 'View Infoblock','ui' => 0])

    ->setLocation('post_type', '==', 'attorneys')
    ->setGroupConfig('hide_on_screen', [
        'the_content'
    ]);
add_action('acf/init', function () use ($cpt_attorneys) {
    acf_add_local_field_group($cpt_attorneys->build());
});
