<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$testimonials = new FieldsBuilder('testimonials');
$testimonials
    // ->addText('title_block', ['label' => 'Title'])
    ->addText('testimonials_title', ['label' => 'Title'])
    ->addWysiwyg('testimonials_text', ['label' => 'Text'])
    ->addNumber('review_rating', ['label' => 'Review Rating' ])
    ->setLocation('post_type', '==', 'testimonials')
    ->setGroupConfig('hide_on_screen', [
        'the_content',
        'the_excerpt'
    ]);
add_action('acf/init', function () use ($testimonials) {
    acf_add_local_field_group($testimonials->build());
});

