<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$perspectives = new FieldsBuilder('perspectives');
$perspectives
    ->addWysiwyg('text_top',['label' => 'Content Top'])
        ->setWidth('50')
    ->addLink('link_top',['label' => 'Link'])
        ->setWidth('50')
  
    ->addWysiwyg('text_bottom',['label' => 'Content Bottom'])
        ->setWidth('50')
    ->addLink('link_bottom',['label' => 'Link'])
        ->setWidth('50')
    // ->conditional('more_on', '==', '1')
    ->addTrueFalse('more_on', ['label' => 'View Infoblock','ui' => 0])

    ->setLocation('post_type', '==', 'perspectives')
    ->setGroupConfig('hide_on_screen', [
        'the_content'
    ]);
add_action('acf/init', function () use ($perspectives) {
    acf_add_local_field_group($perspectives->build());
});
