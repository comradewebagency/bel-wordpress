<?php use StoutLogic\AcfBuilder\FieldsBuilder;

$advanteges = new FieldsBuilder('advanteges');
$advanteges
    ->addImage('user', ['label' => 'User Image'])
        ->setWidth('50')
    ->addImage('image', ['label' => 'Image'])
        ->setWidth('50')
    ->addText('title', ['label' => 'Title'])
        ->setWidth('50')
    ->addWysiwyg('content', ['label' => 'Text'])
        ->setWidth('50')
    ->addWysiwyg('content_bottom', ['label' => 'Text'])
    ->addText('review_title_top', ['label' => 'Title'])
    ->addRepeater('review_list', ['label' => 'Item', 'layout' => 'block'])
        ->addImage('review_image', ['label' => 'Image'])
            ->setWidth('50')
        ->addNumber('review_rating', ['label' => 'Review Rating' ])
        ->addLink('reviews_link', ['label' => 'link'])
    ->endRepeater()

    ->setLocation('page_template', '==', 'pages/advanteges.php')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);
add_action('acf/init', function() use ($advanteges) {
    acf_add_local_field_group($advanteges->build());
});
