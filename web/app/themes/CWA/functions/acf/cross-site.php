<?php
use StoutLogic\AcfBuilder\FieldsBuilder;
$cross_site = new FieldsBuilder('cross-site');
$cross_site
    ->addTab('Contact Us')
        ->addText('contact_us_title', ['label' => 'Title'])
            ->setWidth('50')
        ->addText('contact_us_link', ['label' => 'Link'])
            ->setWidth('50')
        ->addText('contact_us_form_title', ['label' => 'Title'])
            ->setWidth('50')
        ->addNumber('home_form_id', ['label' => 'Form ID'])
    ->addTab('Hero')
        // ->addText('home_hero_slide_title', ['label' => 'Title'])
        // ->addText('home_hero_slide_subtitle', ['label' => 'Subtitle'])
        // ->addLink('home_hero_slide_link', ['label' => 'Link'])
            // ->setWidth('50')
        ->addImage('perspectives_inner', ['label' => 'Image'])
            ->setWidth('50')
    ->addTab('Informarion Block')
        ->addWysiwyg('infoblock_content', ['label' => 'Title'])
            ->setWidth('50')
        ->addLink('infoblock_link', ['label' => 'Link'])
            ->setWidth('50')
    // ->addTab('Perspectives Inner')
        
    ->addTab('Practice Areas Inner')
        ->addText('cases_title', ['label' => 'Title'])
        ->addLink('cases_link', ['label' => 'Link'])
        ->addText('preview_title', ['label' => 'Title'])
        ->addImage('practice_bg', ['label' => 'Image'])
    ->addTab('Navigation Block')
        ->addText('navigation_title',['label' => 'Title'])
            ->setWidth('50')
        ->addLink('navigation_link',['label' => 'Link'])
            ->setWidth('50')
        
    ->setLocation('options_page', '==', 'cross-site-blocks')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);
add_action('acf/init', function() use ($cross_site) {
    acf_add_local_field_group($cross_site->build());
});
