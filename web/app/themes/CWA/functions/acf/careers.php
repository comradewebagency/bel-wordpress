<?php use StoutLogic\AcfBuilder\FieldsBuilder;

$careers = new FieldsBuilder('careers');
$careers
    ->addTab('Content')
        ->addText('title_block', ['label' => 'Title'])
        ->addWysiwyg('content', ['label' => 'Content'])
        ->addImage('image',['label' => 'Image'])
        ->addText('vakansy_link', ['label' => 'Email link'])
        ->addFlexibleContent('vakansy', ['label' => 'Vacancy'])
            ->addLayout('vakansy_layout', ['label' => 'Item'])
                ->addText('vakansy_title', ['label' => 'Title'])
                ->addWysiwyg('description_vakancy', ['label' => 'Description'])
        ->endFlexibleContent()
        // ->addRepeater('vakansy', ['label' => 'Vacancy', 'layout' => 'Link'])
        //     ->addText('vakansy_link', ['label' => 'Email link'])
        //     ->addText('vakansy_title', ['label' => 'Title'])
        //     ->addWysiwyg('description_vakancy', ['label' => 'Description'])
        // ->endRepeater()
    ->addTab('Information')
        ->addWysiwyg('info_content', ['label' => 'Content'])
    
    ->setLocation('page_template', '==', 'pages/careers.php')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);
add_action('acf/init', function() use ($careers) {
    acf_add_local_field_group($careers->build());
});
