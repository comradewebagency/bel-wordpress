<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$practic = new FieldsBuilder('practic');
$practic
    ->addText('title', ['label' => 'Title'])
    ->addWysiwyg('text',['label' => 'Content Text'])
    ->addLink('link', ['label' => 'Link'])
    ->addImage('practice_bg_top', ['label' => 'Top Image'])
    ->addImage('practice_bg', ['label' => 'Bottom Image'])
    ->setLocation('page_template', '==', 'pages/practice-area.php');
add_action('acf/init', function () use ($practic) {
    acf_add_local_field_group($practic->build());
});

