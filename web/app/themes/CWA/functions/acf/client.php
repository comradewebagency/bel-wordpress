<?php
 use StoutLogic\AcfBuilder\FieldsBuilder;
$client = new FieldsBuilder('client');
$client
    ->addTab('Cases')
        ->addText('link_content', ['label' => 'Button'])
        ->addWysiwyg('content', ['label' => 'Content'])
        ->addText('title', ['label' => 'Title'])
        ->addWysiwyg('subtitle', ['label' => 'Subtitle'])
    ->addTab('Testimonials')
        ->addText('testimonials_title',['label' => 'Title'])
        ->addText('link_name', ['label' => 'Button'])

    ->setLocation('page_template', '==', 'pages/client-results.php')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);

add_action('acf/init', function() use ($client) {
    acf_add_local_field_group($client->build());
});