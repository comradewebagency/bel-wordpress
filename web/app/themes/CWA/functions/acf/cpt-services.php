<?php use StoutLogic\AcfBuilder\FieldsBuilder;

$cpt_case = new FieldsBuilder('cpt_case');
$cpt_case
    ->addText('case_studies_item_text', ['label' => 'Text'])
    ->addWysiwyg('case_studies_item_inner_text', ['label' => 'Text'])
    ->setLocation('post_type', '==', 'cases');
    // ->setGroupConfig('hide_on_screen', [
    //     'the_content'
    // ]);
add_action('acf/init', function() use ($cpt_case) {
    acf_add_local_field_group($cpt_case->build());
});
