<?php
 use StoutLogic\AcfBuilder\FieldsBuilder;
$contact = new FieldsBuilder('contact');
$contact
    ->addText('contact_title',['label' => 'Title'])
    ->addText('contact_subtitle', ['label' => 'Subtitle'])
    ->addImage('contact_image', ['label' => 'Image'])

    ->setLocation('page_template', '==', 'pages/contact-us.php')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);

add_action('acf/init', function() use ($contact) {
    acf_add_local_field_group($contact->build());
});