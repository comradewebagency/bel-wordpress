<?php
 use StoutLogic\AcfBuilder\FieldsBuilder;

$common_hero = new FieldsBuilder('common_hero');
$common_hero
    ->addTab('Hero')
        ->addText('home_hero_slide_title', ['label' => 'Title'])
        ->addText('home_hero_slide_subtitle', ['label' => 'Subtitle'])
        ->addImage('home_hero_slide_image', ['label' => 'Image'])
            ->setWidth('50')
        ->setLocation('page_template', '==', 'pages/practice-area.php')
            ->or('page_template', '==', 'pages/attorneys.php')
            ->or('page_template', '==', 'pages/advanteges.php')
            ->or('page_template', '==', 'pages/careers.php')
            ->or('page_template', '==', 'pages/client-results.php')
            ->or('page_template', '==', 'pages/contact-us.php')
            ->or('page_template', '==', 'pages/payment.php')
           
        ->setGroupConfig('hide_on_screen', [
            'featured_image',
            'the_content'
        ]);
add_action('acf/init', function () use ($common_hero) {
    acf_add_local_field_group($common_hero->build());
});

$hero_simple = new FieldsBuilder('hero_simple');
$hero_simple
    ->addImage('home_hero_slide_image', ['label' => 'Image'])
    ->setLocation('post_type', '==', 'attorneys')
        ->or('page_template', '==', 'pages/manager.php')

->setGroupConfig('hide_on_screen', [
    'featured_image',
    'the_content'
]);
add_action('acf/init', function () use ($hero_simple) {
    acf_add_local_field_group($hero_simple->build());
});


$what_we_do = new FieldsBuilder('what_we_do');
$what_we_do
    ->addTab('Service')
        ->addWysiwyg('text',['label' => 'Content Text'])
    ->setLocation('post_type', '==', 'services')
    ->setGroupConfig('hide_on_screen', [
        'featured_image',
        'the_content'
    ]);
add_action('acf/init', function () use ($what_we_do) {
    acf_add_local_field_group($what_we_do->build());
});
