<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$practice_inner = new FieldsBuilder('practice_inner');
$practice_inner
    ->addTab('Hero')
        ->addText('home_hero_slide_subtitle', ['label' => 'Subtitle'])
        ->addImage('home_hero_slide_image', ['label' => 'Image'])
            ->setWidth('50')
    ->addTab('Content')
        ->addWysiwyg('text',['label' => 'Content Text'])
    ->addTab('Case Results')
        ->addRelationship('practice_list', ['label' => 'Client Results', 'post_type' => ['cases'], 'return_format' => 'object',  'max' => '6',])

    ->setLocation('post_type', '==', 'practice');

add_action('acf/init', function () use ($practice_inner) {
    acf_add_local_field_group($practice_inner->build());
});

