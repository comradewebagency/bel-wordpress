<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$theme_settings = new FieldsBuilder('settings');
$theme_settings
    ->addTab('Header', ['placement' => 'left'])
        ->addImage('header_logotype_gorizontal', ['label' => 'Logotype gorizontal'])
        ->addImage('header_logotype_vertical', ['label' => 'Logotype vertical'])
        ->addImage('header_logotype_dark', ['label' => 'Dark Logotype'])
        ->setWidth('50')

    ->addTab('Footer')
    ->addLink('privacy_policy',["label" => "Privacy Policy"])
        ->setWidth('50')
    ->addLink('disclaimer',["label" => "Disclaimer"])
        ->setWidth('50')
    ->addText('footer_copyright')
    ->addTab('Contact Information')
        ->addGoogleMap('map_address')
        ->addText('text_address')

        ->addText('phone_number', ['label' => 'Phone'])
        ->addText('phone_number_two', ['label' => 'Fax'])
        ->addText('email')
            ->setWidth('50')

    ->addRepeater('social_networks', [
        'button_label' => 'Add Social Network',
        'layout' => 'table',
    ])
        ->addSelect('Icon', array(
            'choices' => array(
                'fab fa-facebook' => 'Facebook',
                'fab fa-twitter' => 'Twitter',
                'fab fa-linkedin' => 'LinkedIn',
                'fab fa-google-plus-square' => 'Google'
            )
        ))
        ->addUrl('Url')
    ->endRepeater()
    ->addText('work_hours')
    ->addLink('payment')
    ->setWidth('50')
    ->addlink('client_portal')
    ->setWidth('50')
    ->addLink('link')
    ->setWidth('50')
    ->addTab('Other')
    ->addText('Google_Map_Key')
    ->addGoogleMap('address-map')
    ->addUrl('google_maps_link')
    ->addText('google_maps_link_title')


    ->addTab('Default_Image')
        ->addImage('default_image')
    ->addTab('search',["label"=>"Search Page"])
        ->addText('title_search', ["label"=>'Title'])
    ->addTab('error',["label"=>"Page 404 "])
        ->addLink('error_button', ['label' => 'Button'])
            ->setWidth('50')
        ->addText('error_text', ["label"=>'Content Text'])
            ->setWidth('100')
        // ->addImage('error_bacground', ['label' => 'Background'])
    ->addTab('Practice Areas: Inner')
        ->addPageLink('cases_main_page')
    ->addTab('Top Banner')
        ->addWysiwyg('top_banner_text')

    ->setLocation('options_page', '==', 'theme-general-settings');
add_action('acf/init', function() use ($theme_settings) {
    acf_add_local_field_group($theme_settings->build());
});
