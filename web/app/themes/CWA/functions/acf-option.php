<?php

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
        'update_button' => __('Save Theme Settings', 'acf'),
        'updated_message' => __("Theme Update", 'acf'),
    ));

    acf_add_options_page(array(
        'page_title' => 'Cross Site Blocks',
        'menu_title' => 'Cross Site Blocks',
        'menu_slug' => 'cross-site-blocks',
        'capability' => 'edit_posts',
        'redirect' => false,
        'update_button' => __('Save Cross Site Blocks', 'acf'),
        'updated_message' => __("Blocks Update", 'acf'),
    ));

}
