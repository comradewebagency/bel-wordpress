<?php


function news_inner_description() {
    return strip_tags(get_field('text_top', get_the_ID()));
}

function register_yoast_vars() {
    wpseo_register_var_replacement('%%news_inner_description%%', 'news_inner_description', 'advanced', 'News Inner Description');
}

add_action('wpseo_register_extra_replacements', 'register_yoast_vars');
