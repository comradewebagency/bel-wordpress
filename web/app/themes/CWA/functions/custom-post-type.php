<?php
//add_action( 'init', 'create_product_taxonomies' );
//function create_product_taxonomies(){
//
//    $tags_labels = array(
//        'name' => _x( 'Case Industries', 'taxonomy general name' ),
//        'singular_name' => _x( 'Industries', 'taxonomy singular name' ),
//        'search_items' =>  __( 'Search Industry' ),
//        'all_items' => __( 'All Industries' ),
//        'parent_item' => __( 'Parent Industries' ),
//        'parent_item_colon' => __( 'Parent Industries:' ),
//        'edit_item' => __( 'Edit Industry' ),
//        'update_item' => __( 'Update Industry' ),
//        'add_new_item' => __( 'Add New Industry' ),
//        'new_item_name' => __( 'New Industry' ),
//        'menu_name' => __( 'Industries' ),
//    );
//
//    register_taxonomy('industries', array('industries'), array(
//        'hierarchical' => true,
//        'labels' => $tags_labels,
//        'show_ui' => true,
//        'update_count_callback' => '_update_post_term_count',
//        'query_var' => true,
//        'has_archive'    => false,
//        'rewrite' => array( 'slug' => 'industries', 'with_front' => false ),
//    ));
//
//}


add_action( 'init', 'create_post_type' );

function create_post_type(){
    register_post_type( 'practice',
        array(
            'labels' => array(
                'name' => 'Services',
                'singular_name' => 'Services',
                'add_new'  => 'Add New Service',
                'add_new_item' => 'Add New Service'
            ),

            'publicly_queryable' => true,
            'public'             => true,
            'show_in_nav_menus' => true,
            'show_ui'            => true,
            'hierarchical'       => true,
            'query_var'          => true,
            'menu_position'      => 4,
            'rewrite'            => array('slug' => 'services', 'with_front' => false ),
            'supports'           => array('title','thumbnail'),

        )
    );

    register_post_type( 'cases',
        array(
            'labels' => array(
                'name' => 'Cases',
                'singular_name' => 'Cases',
                'add_new'  => 'Add New Case',
                'add_new_item' => 'Add New Case'
            ),
            'publicly_queryable' => false,
            'public'             => false,
            'show_ui'            => true,
            'hierarchical'       => true,
            'query_var'          => true,
            'menu_position'      => 4,
            'rewrite'            => array( 'with_front' => false ),
            'supports'           => array('title', 'thumbnail',)
        )
    );
    register_post_type( 'attorneys',
        array(
            'labels' => array(
                'name' => 'Lawyers',
                'singular_name' => 'Lawyers',
                'add_new'  => 'Add New Lawyer',
                'add_new_item' => 'Add New Lawyer'
            ),
            'publicly_queryable' => true,
            'public'             => true,
            'show_ui'            => true,
            'hierarchical'       => true,
            'query_var'          => true,
            'menu_position'      => 4,
            'rewrite'            => array( 'slug' => 'lawyers', 'with_front' => false ),
            'supports'           => array('title', 'thumbnail',)
        )
    );

    register_post_type( 'Testimonials',
    array(
        'labels' => array(
            'name' => 'Testimonials',
            'singular_name' => 'Testimonials',
            'add_new'  => 'Add New Testimonials',
            'add_new_item' => 'Add New Testimonials'
        ),
        'publicly_queryable' => true,
        'public'             => false,
        'show_ui'            => true,
        'hierarchical'       => true,
        'query_var'          => true,
        'menu_position'      => 4,
        'rewrite'            => array( 'with_front' => false ),
        'supports'           => array('title', 'excerpt')
    )
);
    register_post_type( 'perspectives',
        array(
            'labels' => array(
                'name' => 'News & Events',
                'singular_name' => 'News',
                'add_new'  => 'Add New News',
                'add_new_item' => 'Add New News'
            ),
            'publicly_queryable' => true,
            'public'             => true,
            'show_ui'            => true,
            'hierarchical'       => true,
            'query_var'          => true,
            'menu_position'      => 4,
            'rewrite'            => array( 'slug' => 'news', 'with_front' => false ),
            'supports'           => array('title', 'thumbnail',)
        )
    );

}
