<?php
/**
 * Template for displaying Client Results Page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Client Results Page
 */
get_header();
get_template_part('blocks/common/block', 'hero');
get_template_part('blocks/сlient-results/block', 'case-studies');
get_template_part('blocks/сlient-results/block', 'testimonials');
get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();
