<?php
/**
 * Template for displaying Atorneys  Page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Atorneys  Page
 */
get_header();
get_template_part('blocks/common/block', 'hero');
get_template_part('blocks/attorneys/attorneys', 'item');

get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();

