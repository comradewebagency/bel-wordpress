<?php
/**
 * Template for displaying Contact us Page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Contact us Page
 */
get_header();
get_template_part('blocks/common/block', 'hero');
get_template_part('blocks/contact-us/block', 'map');

get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();
