<?php
/**
 * Template for displaying Advanteges  Page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Advanteges  Page
 */

get_header();
get_template_part('blocks/common/block', 'hero');
get_template_part('blocks/advanteges/block', 'advanteges');
get_template_part('blocks/advanteges/block', 'review-block');


get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();
