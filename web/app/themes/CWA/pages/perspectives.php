<?php
/**
 * Template for displaying Perspectives
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Perspectives
 */

get_header();
    get_template_part('blocks/perspectives/block', 'hero');
    get_template_part('blocks/perspectives/block', 'cards');
    // get_template_part('blocks/perspectives/preview', 'cases');
    get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();