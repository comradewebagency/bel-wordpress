<?php
/**
 * Template for displaying Practice Area
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Practice Areas
 */

get_header();
    get_template_part('blocks/common/block', 'hero');
    get_template_part('blocks/practice-area/block-preview', 'posts');
    get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();