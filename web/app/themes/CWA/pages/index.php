<?php
/**
 * Template for displaying Home page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Home Page
 */

get_template_part('blocks/home/block', 'hero');
get_template_part('blocks/home/block', 'iconbox');
get_template_part('blocks/home/block', 'description');
get_template_part('blocks/home/block', 'practic-description');
get_template_part('blocks/home/block', 'case-studies');
get_template_part('blocks/home/block', 'testimonials-slider');
get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();

