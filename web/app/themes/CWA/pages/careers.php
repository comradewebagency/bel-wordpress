<?php
/**
 * Template for displaying Careers  Page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 *
 * Template Name: Careers  Page
 */
get_header();
get_template_part('blocks/common/block', 'hero');
get_template_part('blocks/careers/block', 'content');
get_template_part('blocks/careers/block', 'list');
get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();

