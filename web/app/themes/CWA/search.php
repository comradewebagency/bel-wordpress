<?php
// wp_head();
get_header();
 ?>
 <script>
        function stickyHeaderInner() {

        var n = $(".header-inner").height();
        // console.log(n);

        if($(document).scrollTop() > 0){
            $(".hero-js").css("margin-top",0);
        }
        if( $("div").is("#wpadminbar")) {
            var adminPanel = $("#wpadminbar").height();
            $(".hero-js").css("margin-top", n - adminPanel+"px");
        } else {
            $(".hero-js").css("margin-top",n+"px");
        }
        }
        stickyHeaderInner();

        $(document).scroll(function(){
            stickyHeaderInner();
        });   
 </script>
 <?php
 if($_POST['s']) {
    $query = new WP_Query('post_status=publish');
    $query->query_vars['posts_per_page'] = 10;
    $query->query_vars['s'] = $_POST['s'];
    $results = relevanssi_do_query($query);
}

 ?>
 <?php
get_template_part('blocks/cross-site/block', 'hero'); ?>

<style>
    .search-page {
        padding-bottom: 100px;
    }
    .search-page .container {
        display: flex;
        flex-direction: column;
    }
    .search-page .form-horizontal{
        margin-right: auto;
        margin-left: 0;
    }
    .search-result {
        display: flex;
        margin-bottom: 20px;
    }
    .search-result__picture img {
        width: 300px;
        height: 200px;
        object-fit: cover;
        object-position:  center;
        margin-right: 20px;
    }
    @media screen and (max-width: 768px){
        .search-result {
            flex-direction: column;
        }
        .search-result img {
            width: 100%;
            
        }
        .search-result__picture {
            margin-bottom: 0;
        }
        .search-page__title {
            font-size: 28px;
            line-height: 36px
        }
      
    }
</style>
<?php if(isset($results)) { ?>
    <section class="search-page">
        <div class="container">
            <?php if(isset($results) && !empty($results)) { ?>
                <h1 class="h2 search-page__title">Search Results for: “<?php echo $_POST['s'] ?>”</h1>
            <?php } else { ?>
                <h1 class="h2 search-page__title">Results Not Found</h1>
            <?php } ?>
            <div class="form-horizontal"  style="max-width: 550px; width: 100%;" >
                <form role="search" method="POST" action="<?php echo esc_url( home_url( '/' )); ?>"  style="max-width: 550px; width: 100%; display: flex; flex-wrap: nowrap; flex-direction: row!important; align-items: flex-start;  ">
                    <div class="input__wrap">
                        <input class="input" value="<?php echo get_search_query() ?>" name="s" id="s"  type="text" placeholder="Search Perspectives by Keyword">
                    </div>
                    <button style="margin-left: 20px" type="submit"><i class="fal fa-search"></i></button>
                </form>
            </div>
            <div class="search-page__results">
                
                <?php if($query->have_posts()) { ?>
                    <div id="search--results" style="padding-top: 40px;" class="insight-grid -animate-from-top-">
                        <?php
                            while($query->have_posts()) : $query->the_post();
                                $html = new returnSearchResult($post);
                                $html->html();
                            endwhile;
                        ?>
                    </div>
                <?php } ?>
                <?php if (  $query->max_num_pages > 1 ) : ?>
                    <script>
                        var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                        var true_posts = '<?php echo serialize($query->query_vars); ?>';
                        var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                        var max_pages = '<?php echo $query->max_num_pages; ?>';
                    </script>
                    <div class="search-page__more" style="margin-top: 15px;">
                        <div id="true_loadmore" class="btn">load more results</div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php
    // get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();