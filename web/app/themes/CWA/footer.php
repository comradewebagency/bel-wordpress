<?php
$footer_logotype = get_field('footer_logotype', 'option');
$footer_left_link = get_field('footer_left_link', 'option');
$footer_right_link = get_field('footer_right_link', 'option');
$footer_copyright = get_field('footer_copyright', 'option');
$google_maps_link = get_field('google_maps_link', 'option');
$text_address = get_field('text_address', 'option');
$phone_number = get_field('phone_number', 'option');
$work_days = get_field('work_days', 'option');
$work_hours = get_field('work_hours', 'option');
$footer_icons = get_field('footer_icons', 'option');
$privacy_policy_block = get_field('privacy_policy', 'option');
$disclaimer_block = get_field('disclaimer', 'option');
?>

<footer class="footer">
    <div class="container">
        <div class="footer__top-row">
            <div class="footer__top-col">
                <?php get_template_part('blocks/cross-site/footer', 'navigation'); ?>
            </div>
        </div>
        <div class="footer__bottom-row">
            <div class="footer__bottom-col">
                <p class="copyright" style="text-align: left">
                &copy; Copyright <?php echo date('Y'); ?><?php if (!empty($footer_copyright)){?>
                        <?php echo $footer_copyright; ?>
                        <?php } ?>
                <br>All Rights Reserved.
                </p>
            </div>
          <div class="footer__bottom-col">
            <div class="privacy-policy">
                <a href="<?php echo $disclaimer_block['url']?>" target="<?php echo $disclaimer_block['target']?>"><?php echo $disclaimer_block['title'] ?></a><span>|</span><a href="<?php echo $privacy_policy_block['url'] ?>" target="<?php echo $privacy_policy_block['target'] ?>" ><?php echo $privacy_policy_block['title'] ?></a></div>
          </div>
          <div class="footer__bottom-col">
            <div class="study">
              <?php $queried_object = get_queried_object(); ?>
				<style>
					.comradev2 a {
						color: #ffffff;
					}
					.comradev2 a {
						opacity: 0.6;
					}
					.comradev2 span {
						opacity: 0.6;
						color: #ffffff;
					}
					.comradev2 a:hover {
						opacity: 1;
					}
				</style>	
				<div class="comradev2">
					<?php if (is_front_page()) { ?>
						<span>Website by Comrade - </span> <a href="https://comradeweb.com/who-we-serve/law-firms/" rel="dofollow" target="_blank" class="white-link">Law Firm Marketing Agency</a>
					<?php } else if ($queried_object->post_type == 'practice') { ?>
					<?php 
						switch ($queried_object->ID) {
							case 162:
								?><span>Website by</span> <a href="https://comradeweb.com/who-we-serve/law-firms/" rel="dofollow" target="_blank" class="white-link">Comrade - Law Firm Marketing Company</a><?php
									break;
							case 588:
								?><span>Website by</span> <a href="https://comradeweb.com/who-we-serve/law-firms/" rel="dofollow" target="_blank" class="white-link">Comrade - Digital Marketing For Lawyers</a><?php
									break;
							case 132:
								?><span>Website by Comrade - </span> <a href="https://comradeweb.com/blog/internet-marketing-for-bankruptcy-attorneys/" rel="dofollow" target="_blank" class="white-link">Bankruptcy Attorney Marketing</a><?php
									break;
							case 496:
								?><span>Website by</span> <a href="https://comradeweb.com/who-we-serve/law-firms/" rel="dofollow" target="_blank" class="white-link">Comrade - Law Firm Marketing Agency</a><?php
									break;
							case 161:
								?><span>Website by</span> <a href="https://comradeweb.com/who-we-serve/law-firms/" rel="dofollow" target="_blank" class="white-link">Comrade - Lawyer Website Marketing</a><?php
									break;
                            case 130:
								?><span>Website by Comrade - </span> <a href="https://comradeweb.com/blog/internet-marketing-for-bankruptcy-attorneys/" rel="dofollow" target="_blank" class="white-link">Bankruptcy Lawyer Marketing</a><?php
									break;
							default: 
								?><span>Website by</span> <a href="https://comradeweb.com/" rel="nofollow noopener noreferrer" target="_blank" class="white-link">Comrade Digital Marketing Agency</a><?php
									break;
					}
					?>
					<?php } else { ?>
                        <?php
                        switch ($queried_object->ID) {
                            case 98:
								?><span>Website by Comrade - </span> <a href="https://comradeweb.com/internet-marketing-agency-san-francisco/" rel="dofollow" target="_blank" class="white-link">San Francisco Digital Marketing Company</a><?php
									break;
                            case 165:
								?><span>Website by Comrade - </span> <a href="https://comradeweb.com/seo-agency-san-francisco/" rel="dofollow" target="_blank" class="white-link">San Francisco SEO Agency</a><?php
									break;
							default: 
								?><span>Website by</span> <a href="https://comradeweb.com/" rel="nofollow noopener noreferrer" target="_blank" class="white-link">Comrade Digital Marketing Agency</a><?php
									break;
                        }
                        ?>
					<?php } ?>
				</div>
            </div>
          </div>
        </div>
    </div>
    <?php wp_footer();?>
</footer>
<!-- <script>
function delMenu() {
    var sizeWindow = window.innerWidth;
    var footerMenu = document.querySelector(".footer-menu");
    var liMenu = $(footerMenu).children("li");
    if($(window).innerWidth <= 480 ) {
        var newDiv = createElement
        liMenu.each(function(i, item) {
            if(i%2){

            }
        });
    }

}
window.addEventListener('resize', delMenu);

</script> -->
<script>
$("input[type='search']").on('keypress', function() {

  if($(this).val() !== '') {
    $(this).siblings('button').removeAttr("disabled");
  } else {
    $(this).siblings("button").attr("disabled", "true");
  }
})

</script>

</main>

<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js"></script>

<script>
    function stickyHeaderInner() {

var n = $(".header-inner").height();
// console.log(n);

    if($(document).scrollTop() > 0){
        $(".hero-js").css("margin-top",0);
    }
    if( $("div").is("#wpadminbar")) {
        var adminPanel = $("#wpadminbar").height();
        $(".hero-js").css("margin-top", n - adminPanel+"px");
    } else {
        $(".hero-js").css("margin-top",n+"px");
    }
}




$(document).scroll(function(){
stickyHeaderInner();
});
stickyHeaderInner();

    $(document).ready(function(){
    var parentSearch = $(".personal-cabinet .form-search");
    $(".form-search input").blur(function () {
        $(parentSearch).removeClass("parentSerach-active");
        $(this).siblings('button').find('i').css("color",'#fff');
    });
    $(".form-search input").hover(function (e) {
        $(e.target).siblings('button').find('i').css("color",'#3A6EAA');
        $(parentSearch).addClass("parentSerach-active");
        $(e.target).focus();
    });
})



$(document).ready(function(){
    $('.input').blur(function(e) {
        if($(this).val()){
            $(e.target).closest(".input__wrap").addClass('not-empty');
        } else{
            $(e.target).closest(".input__wrap").removeClass('not-empty');
        }
    });
    $('.input').focus(function(e) {
        $(this).closest(".input__wrap").addClass("not-empty");
    })
    var tel = $('input[type=tel]');

if (tel.length) {
    tel.each(function (i) {
        new Cleave(tel[i], {
            blocks: [3, 3, 4],
            delimiters: [' ', '-', '-'],
            numericOnly: true
        })
    });
}
})


</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.3/jquery.rateit.js"></script>
<!-- <script>
    var el = document.querySelectorAll('img[data-src]');
    window.addEventListener('load', function() {
    var observer = Lazy();
    observer.observe();
    })
</script> -->
</body>

</html>
