/* eslint-disable */
'use strict';
const fs = require('fs');
const mkdirp = require('mkdirp');
const blockName = process.argv[2];
const defaultExtensions = ['scss', 'pug', 'js', 'assets'];

const extensions = uniqueArray(defaultExtensions);

if (blockName) {
  const dirPath = `src/components/${blockName}/`;
  mkdirp(dirPath, (err) => {
    if (err) {
      console.error(`[CWA] Cancel operation: ${err}`);
    }
    else {
      console.log(`[CWA] Folder creation: ${dirPath} (if absent)`);

      extensions.forEach((extension) => {
        const filePath = `${dirPath + blockName}.component.${extension}`;
        let fileContent = '';
        let fileCreateMsg = '';

        if (extension === 'scss') {
          fileContent = ``;
          // fileCreateMsg = '';
        }

        else if (extension === 'js') {
          fileContent = `import './${blockName}.component.scss';\nimport './${blockName}.component.pug';`;
        }

        else if (extension === 'md') {
          fileContent = '';
        }

        else if (extension === 'pug') {
          fileContent = `mixin ${blockName}(data)\n  .${blockName}`;
        }

        else if (extension === 'img') {
          const imgFolder = `${dirPath}img/`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`[CWA] Creating a folder: ${imgFolder} (if absent)`);
            });
          } else {
            console.log(`[CWA] (already exists) `);
          }
        }

        else if (extension === 'bg-img') {
          const imgFolder = `${dirPath}bg-img/`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`[CWA] Creating a folder: ${imgFolder} (if absent)`);
            });
          } else {
            console.log(`[CWA] The ${imgFolder} folder is NOT created (already exists) `);
          }
        }

        else if (extension === 'assets') {
          const imgFolder = `${dirPath}assets/img`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`[CWA] Creating a folder: assets (if absent)`);
            });
          } else {
            console.log(`[CWA] The assets folder is NOT created (already exists) `);
          }
        }

        if (fileExist(filePath) === false && extension !== 'assets' && extension !== 'img' && extension !== 'bg-img' && extension !== 'md') {
          fs.writeFile(filePath, fileContent, (err) => {
            if (err) {
              return console.log(`[CWA] File NOT created:: ${err}`);
            }
            console.log(`[CWA] File created: ${filePath}`);
            if (fileCreateMsg) {
              console.warn(fileCreateMsg);
            }
          });
        }
        else if (extension !== 'img' && extension !== 'assets' && extension !== 'bg-img' && extension !== 'md') {
          console.log(`[CWA] File NOT created:: ${filePath} (already exists)`);
        }
        else if (extension === 'md') {
          fs.writeFile(`${dirPath}readme.md`, fileContent, (err) => {
            if (err) {
              return console.log(`[CWA] File NOT created:: ${err}`);
            }
            console.log(`[CWA] File created: ${dirPath}readme.md`);
            if (fileCreateMsg) {
              console.warn(fileCreateMsg);
            }
          });
        }
      });

    }
  });
} else {
  console.log('[CWA] Cancel operation: component name not specified');
}



function uniqueArray(arr) {
  const objectTemp = {};
  for (let i = 0; i < arr.length; i++) {
    const str = arr[i];
    objectTemp[str] = true;
  }
  return Object.keys(objectTemp);
}

function fileExist(path) {
  const fs = require('fs');
  try {
    fs.statSync(path);
  } catch (err) {
    return !(err && err.code === 'ENOENT');
  }
}