const path = require("path");
const glob = require("glob");
const fs = require("fs");
const webpack = require("webpack");
const autoprefixer = require("autoprefixer");
const TerserPlugin = require("terser-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const {
  CleanWebpackPlugin,
} = require("clean-webpack-plugin/dist/clean-webpack-plugin");
const SVGSpritemapPlugin = require("svg-spritemap-webpack-plugin");

function getPugTemplate() {
  fs.readFile("test.js", (err, data) => {
    const json_data = JSON.parse(data);
    // console.log(json_data);
    json_data.forEach((item) => {
      // console.log(item);
      development.plugins.push(
        new HtmlWebpackPlugin({
          filename: item.filename,
          template: item.template,
          chunks: item.chunks,
        })
      );
    });
  });
}

function _makePugTemplate() {
  let pages = glob.sync(__dirname + "/src/pages/**/*.pug");
  pages.forEach(function (file) {
    let base = path.basename(file, ".pug");
    development.plugins.push(
      new HtmlWebpackPlugin({
        filename: "./" + base + ".html",
        template: `src/pages/${base}/${base}.pug`,
        chunks: [`${base}`, "runtime"],
      })
    );

    production.plugins.push(
      new HtmlWebpackPlugin({
        filename: "./" + base + ".html",
        template: `src/pages/${base}/${base}.pug`,
        chunks: [`${base}`, "runtime"],
      })
    );
  });
}

function _makeEntry() {
  let pages = glob.sync(__dirname + "/src/pages/**/*.pug");
  pages.forEach(function (file) {
    let base = path.basename(file, ".pug");
    development.entry[`${base}`] = [
      path.resolve(__dirname, `src/pages/${base}/${base}.js`),
    ];
    production.entry[`${base}`] = path.resolve(
      __dirname,
      `src/pages/${base}/${base}.js`
    );
  });
}

function _makeAlias() {
  let pages = glob.sync(__dirname + "/src/components/**/*.component.js");
  pages.forEach(function (file) {
    let base = path.basename(file, ".component.js");
    development.resolve.alias[`${capitalizeFirstLetter(base)}`] = path.resolve(
      __dirname,
      `src/components/${base}/`
    );
    production.resolve.alias[`${capitalizeFirstLetter(base)}`] = path.resolve(
      __dirname,
      `src/components/${base}/`
    );
  });
}

function _makePugIncludes() {
  let fileContent = "//Do not edit this file! Changes will be overwritten.\n";
  let pages = glob.sync(__dirname + "/src/components/**/*.component.pug");
  pages.forEach(function (file) {
    let base = path.basename(file, ".component.pug");
    fileContent += `mixin ${capitalizeFirstLetter(
      base
    )}(data)\n  include ../../components/${base}/${base}.component.pug\n  +${base}(data)\n\n`;
  });

  if (fileContent.length) {
    writeFile(fileContent);
  }
}

function writeFile(content) {
  fs.writeFileSync(
    __dirname + `/src/helpers/markup/_include-components.pug`,
    content,
    (err) => {
      if (err) {
        return console.log(`ERROR ${err}`);
      }
      console.log(`Pug includes components created: /src/`);
    }
  );
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const development = {
  entry: {},
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "assets/js/[name].js",
    chunkFilename: "assets/js/[name].js",
    publicPath: "/app/themes/CWA/front/dist/",
  },
  resolve: {
    alias: {},
  },
  optimization: {
    runtimeChunk: "single",
  },
  stats: {
    timings: true,
    all: false,
    modules: false,
    errors: true,
    warnings: false,
    moduleTrace: false,
    errorDetails: true,
    colors: true,
  },
  devServer: {
    host: "0.0.0.0",
    contentBase: path.join(__dirname, "dist/"),
    compress: true,
    inline: true,
    writeToDisk: true,
    overlay: true,
    disableHostCheck: true,
    clientLogLevel: "error",
    port: 7000,
    useLocalIp: true,
    open: true,
    hot: true,
  },
  devtool: "source-map",
  plugins: [
    // dist/assets/img/*.svg
    new SVGSpritemapPlugin("src/components/**/assets/img/*.svg", {
      // styles: path.join(__dirname, 'src/scss/_sprites.scss'),
      output: {
        filename: path.join(__dirname, "dist/assets/img/svg-sprite.svg"),
      },
    }),
    // new HardSourceWebpackPlugin(),
    // new HardSourceWebpackPlugin.ExcludeModulePlugin([
    //   {
    //     // HardSource works with mini-css-extract-plugin but due to how
    //     // mini-css emits assets, assets are not emitted on repeated builds with
    //     // mini-css and hard-source together. Ignoring the mini-css loader
    //     // modules, but not the other css loader modules, excludes the modules
    //     // that mini-css needs rebuilt to output assets every time.
    //     test: /mini-css-extract-plugin[\\/]dist[\\/]loader/,
    //   },
    // ]),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.$": "jquery",
      "window.jQuery": "jquery",
    }),
    new webpack.HashedModuleIdsPlugin({
      hashFunction: "md4",
      hashDigest: "base64",
      hashDigestLength: 8,
    }),
    new CopyWebpackPlugin([
      {
        from: "src/components/**/assets/img/*.*",
        to: "assets/img/",
        flatten: true,
      },
      // {
      //   from:'src/assets/svg-sprite.svg',
      //   to:'assets/img/rel',
      // }
    ]),
    new MiniCssExtractPlugin({
      filename: "assets/css/[name].css",
      chunkFilename: "assets/css/[name].styles.css",
    }),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
          },
          {
            loader: "eslint-loader",
          },
        ],
      },
      {
        test: /\.(woff2)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "assets/fonts/",
              publicPath: "../fonts/",
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192,
              fallback: {
                loader: "file-loader",
                options: {
                  name: "[name].[ext]",
                  outputPath: "assets/img/",
                  publicPath: "../img/",
                },
              },
            },
          },
        ],
      },
      {
        test: /\.pug$/,
        use: [
          {
            loader: "pug-loader",
            options: {
              root: path.resolve(__dirname, "src/"),
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: true,
              reloadAll: true,
              publicPath: "dist/",
            },
          },
          {
            loader: "css-loader",
            options: {},
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [autoprefixer()],
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              data: `@import "helpers/styles/variables.scss"; @import "helpers/styles/mixins.scss";`,
              includePaths: [path.resolve(__dirname, "src/")],
            },
          },
        ],
      },
    ],
  },
};
const production = {
  entry: {},
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "assets/js/[name].js",
    chunkFilename: "assets/js/[name].js",
    // publicPath: '/app/themes/CWA/front/dist/'
  },
  resolve: {
    alias: {},
  },
  optimization: {
    runtimeChunk: "single",
    minimizer: [
      new OptimizeCSSAssetsPlugin({
        cssProcessorPluginOptions: {
          preset: [
            "default",
            {
              discardComments: {
                removeAll: true,
              },
            },
          ],
        },
      }),
      new TerserPlugin({
        parallel: true,
        cache: true,
        terserOptions: {
          output: {
            comments: false,
          },
        },
      }),
    ],
  },
  plugins: [
    // new SVGSpritemapPlugin('src/components/**/assets/img/*.svg', {
    //   // st\yles: path.join(__dirname, 'src/scss/_sprites.scss'),
    //   output: {
    //     filename: path.resolve(__dirname, '/assets/img/svg-sprite.svg')
    //   }
    // }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.$": "jquery",
      "window.jQuery": "jquery",
    }),
    new webpack.HashedModuleIdsPlugin({
      hashFunction: "md4",
      hashDigest: "base64",
      hashDigestLength: 8,
    }),
    new CopyWebpackPlugin([
      {
        from: "src/components/**/assets/img/*.*",
        to: "assets/img/",
        flatten: true,
      },
    ]),
    new MiniCssExtractPlugin({
      filename: "assets/css/[name].css",
      chunkFilename: "assets/css/[name].styles.css",
    }),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
          },
          {
            loader: "eslint-loader",
          },
        ],
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "assets/fonts/",
              publicPath: "../fonts/",
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192,
              fallback: {
                loader: "file-loader",
                options: {
                  name: "[name].[ext]",
                  outputPath: "assets/img/",
                  publicPath: "../img/",
                },
              },
            },
          },
        ],
      },
      {
        test: /\.pug$/,
        use: [
          {
            loader: "pug-loader",
            options: {
              pretty: true,
              root: path.resolve(__dirname, "src/"),
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "dist/",
            },
          },
          {
            loader: "css-loader",
            options: {},
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [autoprefixer()],
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              data: `@import "helpers/styles/variables.scss"; @import "helpers/styles/mixins.scss";`,
              includePaths: [path.resolve(__dirname, "src/")],
            },
          },
        ],
      },
    ],
  },
};

// _makePugIncludes();
_makeAlias();
_makeEntry();
_makePugTemplate();
// getPugTemplate();

module.exports = (env, argv) => {
  if (argv.mode === "development") {
    console.info("development");
    return development;
  }
  if (argv.mode === "production") {
    console.info("production");
    return production;
  }
};

// const path = require('path');
// const glob = require("glob");
// const fs = require('fs');
// const webpack = require("webpack");
// const autoprefixer = require('autoprefixer');
// const TerserPlugin = require('terser-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
// const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin/dist/clean-webpack-plugin');
// const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

// function getPugTemplate() {
//   fs.readFile('test.js', (err, data) => {
//     const json_data = JSON.parse(data);
//     // console.log(json_data);
//     json_data.forEach(item => {
//       // console.log(item);
//       development.plugins.push(
//         new HtmlWebpackPlugin({
//           filename: item.filename,
//           template: item.template,
//           chunks: item.chunks
//         })
//       );
//     });
//   });
// }

// function _makePugTemplate() {
//   let pages = glob.sync(__dirname + '/src/pages/**/*.pug');
//   pages.forEach(function (file) {
//     let base = path.basename(file, '.pug');
//     development.plugins.push(
//       new HtmlWebpackPlugin({
//         filename: './' + base + '.html',
//         template: `src/pages/${base}/${base}.pug`,
//         chunks: [`${base}`,'runtime']
//       })
//     );

//     production.plugins.push(
//       new HtmlWebpackPlugin({
//         filename: './' + base + '.html',
//         template: `src/pages/${base}/${base}.pug`,
//         chunks: [`${base}`,'runtime']
//       })
//     );
//   });
// }

// function _makeEntry() {
//   let pages = glob.sync(__dirname + '/src/pages/**/*.pug');
//   pages.forEach(function (file) {
//     let base = path.basename(file, '.pug');
//     development.entry[`${base}`] = [path.resolve(__dirname, `src/pages/${base}/${base}.js`)];
//     production.entry[`${base}`] = path.resolve(__dirname, `src/pages/${base}/${base}.js`);
//   });
// }

// function _makeAlias() {
//   let pages = glob.sync(__dirname + '/src/components/**/*.component.js');
//   pages.forEach(function (file) {
//     let base = path.basename(file, '.component.js');
//     development.resolve.alias[`${capitalizeFirstLetter(base)}`] = path.resolve(__dirname, `src/components/${base}/`);
//     production.resolve.alias[`${capitalizeFirstLetter(base)}`] = path.resolve(__dirname, `src/components/${base}/`);
//   });
// }

// function _makePugIncludes() {
//   let fileContent = '//Do not edit this file! Changes will be overwritten.\n';
//   let pages = glob.sync(__dirname + '/src/components/**/*.component.pug');
//   pages.forEach(function (file) {
//     let base = path.basename(file, '.component.pug');
//     fileContent += `mixin ${capitalizeFirstLetter(base)}(data)\n  include ../../components/${base}/${base}.component.pug\n  +${base}(data)\n\n`;
//   });

//   if (fileContent.length) {
//     writeFile(fileContent);
//   }
// }

// function writeFile(content) {
//   fs.writeFileSync(__dirname + `/src/helpers/markup/_include-components.pug`, content, (err) => {
//     if (err) {
//       return console.log(`ERROR ${err}`);
//     }
//     console.log(`Pug includes components created: /src/`);
//   });
// }

// function capitalizeFirstLetter(string) {
//   return string.charAt(0).toUpperCase() + string.slice(1);
// }

// const development = {
//   entry: {},
//   output: {
//     path: path.resolve(__dirname, 'dist'),
//     filename: "assets/js/[name].js",
//     chunkFilename: 'assets/js/[name].js',
//   },
//   resolve: {
//     alias: {
//     }
//   },
//   optimization: {
//     runtimeChunk: 'single',
//   },
//   stats: {
//     timings: true,
//     all: false,
//     modules: false,
//     errors: true,
//     warnings: true,
//     moduleTrace: false,
//     errorDetails: true,
//     colors: true,
//   },
//   devServer: {
//     host: '0.0.0.0',
//     contentBase: path.join(__dirname, 'dist/'),
//     compress: true,
//     inline: true,
//     writeToDisk: false,
//     overlay: true,
//     // clientLogLevel: 'warning',
//     port: 7000,
//     open: true,
//     hot: true,
//     useLocalIp: true
//   },
//   devtool: 'source-map',
//   plugins: [
//     // dist/assets/img/*.svg
//     new SVGSpritemapPlugin('src/components/**/assets/img/*.svg', {
//       // styles: path.join(__dirname, 'src/scss/_sprites.scss'),
//       output: {
//         filename: path.join(__dirname, 'dist/assets/img/svg-sprite.svg')
//       }
//     }),
//     // new HardSourceWebpackPlugin(),
//     // new HardSourceWebpackPlugin.ExcludeModulePlugin([
//     //   {
//     //     // HardSource works with mini-css-extract-plugin but due to how
//     //     // mini-css emits assets, assets are not emitted on repeated builds with
//     //     // mini-css and hard-source together. Ignoring the mini-css loader
//     //     // modules, but not the other css loader modules, excludes the modules
//     //     // that mini-css needs rebuilt to output assets every time.
//     //     test: /mini-css-extract-plugin[\\/]dist[\\/]loader/,
//     //   },
//     // ]),
//     new webpack.ProvidePlugin({
//       $: 'jquery',
//       jQuery: 'jquery',
//       'window.$': 'jquery',
//       'window.jQuery': 'jquery',
//     }),
//     new webpack.HashedModuleIdsPlugin({
//       hashFunction: 'md4',
//       hashDigest:'base64',
//       hashDigestLength: 8,
//     }),
//     new CopyWebpackPlugin([
//       {
//         from:'src/components/**/assets/img/*.*',
//         to:'assets/img/',
//         flatten: true,
//       },
//       // {
//       //   from:'src/assets/svg-sprite.svg',
//       //   to:'assets/img/rel',
//       // }
//     ]),
//     new MiniCssExtractPlugin({
//       filename: 'assets/css/[name].css',
//       chunkFilename: 'assets/css/[name].styles.css',
//     }),
//     new CleanWebpackPlugin({
//       cleanStaleWebpackAssets: false,
//     }),
//   ],
//   module: {
//     rules: [
//       {
//         test: /\.js$/,
//         exclude: /node_modules/,
//         use: [
//           {
//             loader: "babel-loader"
//           },
//           {
//             loader: "eslint-loader"
//           }
//         ]
//       },
//       {
//         test: /\.(woff2)$/,
//         use: [
//           {
//             loader: 'file-loader',
//             options: {
//               name: '[name].[ext]',
//               outputPath: 'assets/fonts/',
//               publicPath: '../fonts/'
//             }
//           }
//         ]
//       },
//       {
//         test: /\.(png|jpe?g|gif)$/,
//         use: [
//           {
//             loader: 'url-loader',
//             options: {
//               limit: 8192,
//               fallback: {
//                 loader: 'file-loader',
//                 options: {
//                   name: '[name].[ext]',
//                   outputPath: 'assets/img/',
//                   publicPath: '../img/'
//                 },
//               }
//             },
//           },
//         ],
//       },
//       {
//         test: /\.pug$/,
//         use: [
//           {
//             loader: 'pug-loader',
//             options: {
//               root: path.resolve(__dirname, 'src/')
//             }
//           }
//         ],
//       },
//       {
//         test: /\.scss$/,
//         use: [
//           {
//             loader: MiniCssExtractPlugin.loader,
//             options: {
//               hmr: true,
//               reloadAll: true,
//               publicPath: 'dist/',
//             },
//           },
//           {
//             loader: "css-loader",
//             options: {
//             }
//           },
//           {
//             loader: 'postcss-loader',
//             options: {
//               plugins: [
//                 autoprefixer(),
//               ],
//             }
//           },
//           {
//             loader: "sass-loader",
//             options: {
//               sourceMap: true,
//               data: `@import "helpers/styles/variables.scss"; @import "helpers/styles/mixins.scss";`,
//               includePaths: [
//                 path.resolve(__dirname, "src/")
//               ]
//             }
//           },
//         ]
//       }
//     ]
//   }
// };
// const production = {
//   entry: {},
//   output: {
//     path: path.resolve(__dirname, 'dist'),
//     filename: "assets/js/[name].js",
//     chunkFilename: 'assets/js/[name].js',
//   },
//   resolve: {
//     alias: {
//     }
//   },
//   optimization: {
//     runtimeChunk: 'single',
//     minimizer: [
//       new OptimizeCSSAssetsPlugin({
//         cssProcessorPluginOptions: {
//           preset: ['default', { discardComments: { removeAll: true } }],
//         }
//       }),
//       new TerserPlugin({
//         parallel: true,
//         cache: true,
//         terserOptions: {
//           output: {
//             comments: false,
//           },
//         },
//       })
//     ],
//   },
//   plugins: [
//     new SVGSpritemapPlugin('src/components/**/assets/img/*.svg', {
//       // styles: path.join(__dirname, 'src/scss/_sprites.scss'),
//       output: {
//         filename: path.resolve(__dirname, '/assets/img/svg-sprite.svg')
//       }
//     }),
//     new webpack.ProvidePlugin({
//       $: 'jquery',
//       jQuery: 'jquery',
//       'window.$': 'jquery',
//       'window.jQuery': 'jquery',
//     }),
//     new webpack.HashedModuleIdsPlugin({
//       hashFunction: 'md4',
//       hashDigest:'base64',
//       hashDigestLength: 8,
//     }),
//     new CopyWebpackPlugin([
//       {
//         from:'src/components/**/assets/img/*.*',
//         to:'assets/img/',
//         flatten: true,
//       }
//     ]),
//     new MiniCssExtractPlugin({
//       filename: 'assets/css/[name].css',
//       chunkFilename: 'assets/css/[name].styles.css',
//     }),
//     new CleanWebpackPlugin({
//       cleanStaleWebpackAssets: false,
//     }),
//   ],
//   module: {
//     rules: [
//       {
//         test: /\.js$/,
//         exclude: /node_modules/,
//         use: [
//           {
//             loader: "babel-loader"
//           },
//           {
//             loader: "eslint-loader"
//           }
//         ]
//       },
//       {
//         test: /\.(eot|ttf|woff|woff2)$/,
//         use: [
//           {
//             loader: 'file-loader',
//             options: {
//               name: '[name].[ext]',
//               outputPath: 'assets/fonts/',
//               publicPath: '../fonts/'
//             }
//           }
//         ]
//       },
//       {
//         test: /\.(png|jpe?g|gif)$/,
//         use: [
//           {
//             loader: 'url-loader',
//             options: {
//               limit: 8192,
//               fallback: {
//                 loader: 'file-loader',
//                 options: {
//                   name: '[name].[ext]',
//                   outputPath: 'assets/img/',
//                   publicPath: '../img/'
//                 },
//               }
//             },
//           },
//         ],
//       },
//       {
//         test: /\.pug$/,
//         use: [
//           {
//             loader: 'pug-loader',
//             options: {
//               pretty: true,
//               root: path.resolve(__dirname, 'src/')
//             }
//           }
//         ],
//       },
//       {
//         test: /\.scss$/,
//         use: [
//           {
//             loader: MiniCssExtractPlugin.loader,
//             options: {
//               publicPath: 'dist/',
//             },
//           },
//           {
//             loader: "css-loader",
//             options: {
//             }
//           },
//           {
//             loader: 'postcss-loader',
//             options: {
//               plugins: [
//                 autoprefixer(),
//               ],
//             }
//           },
//           {
//             loader: "sass-loader",
//             options: {
//               sourceMap: true,
//               data: `@import "helpers/styles/variables.scss"; @import "helpers/styles/mixins.scss";`,
//               includePaths: [
//                 path.resolve(__dirname, "src/")
//               ]
//             }
//           },
//         ]
//       }
//     ]
//   }
// };

// // _makePugIncludes();
// _makeAlias();
// _makeEntry();
// _makePugTemplate();
// // getPugTemplate();

// module.exports = (env, argv) => {
//   if (argv.mode === 'development') {
//     console.info('development');
//     return development;
//   }
//   if (argv.mode === 'production') {
//     console.info('production');
//     return production;
//   }
// };
