import './portal.scss';
import '../../helpers/styles/base.scss';
import './portal.pug';
import '../../components/header/header.component';
import '../../components/lazy-load/lazy-load.component';
import '../../components/content-block/content-block.component';
import '../../components/hero/hero.component';

import '../../components/footer/footer.component';