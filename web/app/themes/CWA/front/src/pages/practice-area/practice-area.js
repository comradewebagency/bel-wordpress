import "./practice-area.scss";
import "../../helpers/styles/base.scss";
import "./practice-area.pug";

import "../../components/header/header.component";
import "../../components/hero/hero.component";
import "../../components/lazy-load/lazy-load.component";
import "../../components/contact-us-block/contact-us-block.component";
import "../../components/footer/footer.component";
import "../../components/practic-description/practic-description.component";
import "../../components/list-content-block/list-content-block.component";
import "../../components/info-block/info-block.component";
import Lazy from "../../../node_modules/jquery-lazy/jquery.lazy";

$("img[data-src]").Lazy();
$("source[data-src]").Lazy();
