import "./attorneys-inner.scss";
import "../../helpers/styles/base.scss";
import "./attorneys-inner.pug";
import "../../components/header/header.component";
import "../../components/footer/footer.component";
import "../../components/hero/hero.component";
import "../../components/attorneys-inner/attorneys-inner.component";
// import '../../components/practic--description/practic--description.component';
import "../../components/info-block/info-block.component";
import "../../components/lazy-load/lazy-load.component";
import "../../components/contact-us-block/contact-us-block.component";
import "../../../node_modules/page-scroll-to-id/jquery.malihu.PageScroll2id";
function initScrollLinks() {
  if (window.scrollOffset) {
    $("a[rel='m_PageScroll2id']").mPageScroll2id({
      offset: (window.scrollOffset || 200) + 24,
    });
    return true;
  } else {
    setTimeout(initScrollLinks, 100);
    return false;
  }
}
$(document).ready(() => {
  initScrollLinks();
});

// function stickyMenu(){
//     if ($(window).outerWidth() >= 991) {
//     var a = document.querySelector('.attorneys-menu'), headerHeight = document.querySelector('.header-inner').offsetHeight, b = null, P = 0;
//     window.addEventListener('scroll', Ascroll, false);
//     document.body.addEventListener('scroll', Ascroll, false);
//     function Ascroll() {
//         if (b == null) {
//             var Sa = getComputedStyle(a, ''), s = '';
//             for (var i = 0; i < Sa.length; i++) {
//                 if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
//                     s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
//                 }
//             }
//             if(a.firstChild == "DIV") {
//                 return false;
//             } else {
//                 b = document.createElement('div');
//             }
//
//             b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
//             a.insertBefore(b, a.firstChild);
//             var l = a.childNodes.length;
//             for (var i = 1; i < l; i++) {
//                 b.appendChild(a.childNodes[1]);
//             }
//             a.style.height = b.getBoundingClientRect().height + 'px';
//             a.style.padding = '0';
//             a.style.border = '0';
//         }
//         var Ra = a.getBoundingClientRect(),
//             R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.info-block--blue').getBoundingClientRect().top + 0 + headerHeight - 10);
//         if ((Ra.top - P) <= 0) {
//             if ((Ra.top - P) <= R) {
//             b.className = 'stop';
//             b.style.top = - R + headerHeight +'px';
//             } else {
//             b.className = 'sticky';
//             b.style.top = P + headerHeight +'px';
//             }
//         } else {
//         b.className = '';
//         b.style.top = '';
//     }
//         window.addEventListener('resize', function() {
//             a.children[0].style.width = getComputedStyle(a, '').width
//         }, false);
//     }
// }
// }
// stickyMenu();
// $(window).resize(function() {
//     stickyMenu();
// });

// document.querySelector('button').onclick = function() {
//     var c = this.getBoundingClientRect(),
//         scrolltop = document.body.scrollTop + c.top,
//         scrollleft = document.body.scrollLeft + c.left;
//     alert('top:' + scrolltop + ' left: ' + scrollleft + '');
//   }
// function stickyMenu() {
//     var n = $(".header-inner").height();
//     var hj = $(".hero-js").height();
//     var hp = $(".hero__pagination-wrap").height();
//     var cu = $(".card-user").height();
//     var all = n + hj+ hp + cu - 150;
//     var infoBLock = document.querySelector(".attorneys-inner__col .info-block").getBoundingClientRect();
//     var menu = document.querySelector(".attorneys-menu").getBoundingClientRect();
//     // console.log(menu);
//     var heightElem = $(".attorneys-menu").height();
//     var windowHeight = $(window).height();
//     // // var menuH = menu.height + menu.top;
//     // // console.log(menuH+"px");
//     // function checkSize() {
//     //     if (heightElem > windowHeight ) {
//     //       $('elem').addClass('big');
//     //     } else {
//     //       $('elem').removeClass('big');
//     //     }
//     //   }
//     // if($(document).scrollTop() == menu.top){
//     //     $(".attorneys-menu").addClass("fix-menu");

//     // }
//     // if( menuH == infoBLock.y) {
//     //     $(".attorneys-menu").hide();
//     // }
//     // if($(document).scrollTop() <= all){
//     //     $(".attorneys-menu").removeClass("fix-menu");
//     // }
//     // // if( $("div").is("#wpadminbar")) {
//     // //     var adminPanel = $("#wpadminbar").height();
//     // //     $(".hero-js").css("margin-top", n - adminPanel+"px");
//     // // } else {
//     // //     $(".hero-js").css("margin-top",n+"px");
//     // // }
// }
// stickyMenu();

// $(document).scroll(function(){
//     stickyMenu();
// });
