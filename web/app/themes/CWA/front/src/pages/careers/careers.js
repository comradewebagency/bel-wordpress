import "./careers.scss";
import "../../helpers/styles/base.scss";
import "./careers.pug";

import "../../components/header/header.component";
import "../../components/info-block/info-block.component";
import "../../components/hero/hero.component";
import "../../components/contact-us-block/contact-us-block.component";
import "../../components/footer/footer.component";
import "../../components/careers-block/careers-block.component";
import "../../components/practic-description/practic-description.component";
import "../../components/list-content-block/list-content-block.component";
import Lazy from "../../../node_modules/jquery-lazy/jquery.lazy";

$("img[data-src]").Lazy();
$("source[data-src]").Lazy();
