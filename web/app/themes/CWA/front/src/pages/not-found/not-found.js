import './not-found.scss';
import '../../helpers/styles/base.scss';
import './not-found.pug';
import '../../components/header/header.component';
import '../../components/lazy-load/lazy-load.component';
import '../../components/hero/hero.component';

import '../../components/footer/footer.component';