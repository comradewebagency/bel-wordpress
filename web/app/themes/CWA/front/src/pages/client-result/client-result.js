import "./client-result.scss";
import "../../helpers/styles/base.scss";
import "./client-result.pug";
import "../../components/header/header.component";
import "../../components/hero/hero.component";
import "../../components/lazy-load/lazy-load.component";
import "../../components/description-block/description-block.component";
import "../../components/client-testimonials/client-testimonials.component";
import "../../components/case-studies/case-studies.component";
import "../../components/review-block/review-block.component";
import "../../components/why-choose-us-block/why-choose-us-block.component";
import "../../components/contact-us-block/contact-us-block.component";
import "../../components/footer/footer.component";
import Lazy from "../../../node_modules/jquery-lazy/jquery.lazy";

window.cardReverse = () => {
  console.warn("remove cardReverse call, outdated");
};
$(document).ready(function () {
  $(".case-studies").on("click", ".case-studies__item", function () {
    if ($(this).hasClass("case-studies__item--js")) {
      $(this).removeClass("case-studies__item--js");
    } else {
      $(".case-studies__item--js").removeClass("case-studies__item--js");
      $(this).addClass("case-studies__item--js");
    }
  });
});
