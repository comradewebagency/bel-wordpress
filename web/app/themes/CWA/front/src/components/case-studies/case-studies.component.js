import "./case-studies.component.scss";
import "./case-studies.component.pug";
import Swiper from "swiper";
function getRows(element) {
  var height = element.height();
  var line_height = element.css("line-height");
  line_height = parseFloat(line_height);
  var rows = height / line_height;
  return Math.round(rows);
}
function swiperSlide() {
  // casesSlider = new Swiper('.case-studies__container-js',{
  //     slidesPerView: 'auto',
  //     spaceBetween: 30,
  //     width: 300,
  //     centeredSlides: true
  // });
  if ($(window).outerWidth() <= 480) {
    let casesSlider = new Swiper(".case-studies__container-js", {
      slidesPerView: "auto",
      // centeredSlides: true,
      // spaceBetween: 30
      //     spaceBetween: 30,
      //     width: 300,
      //     centeredSlides: true
    });
  } else {
    try {
      if (casesSlider) {
        casesSlider.destroy(true, false);
      }
    } catch (e) {}
  }
}
window.addEventListener("resize", swiperSlide);
if ($(window).outerWidth() <= 991) {
  window.addEventListener("load", swiperSlide);
}

window.casesFontSizeCorrect = function () {
  $(".case-studies__item-middle").each(function () {
    const element = $(this);
    const text = element.find(".h1");
    const lines = getRows(text);
    let offset = lines / 10;
    console.log(offset);
    if (offset > 1) {
      text.css("font-size", `${47 / offset}px`);
    }
  });
};
window.casesFontSizeCorrect();
