import './review-block.component.scss';
import './review-block.component.pug';
// import Swiper from 'swiper';

// let reviewSwiper= new Swiper('.review-block__slider-js', {
//     spaceBetween: 80,
//     slidesPerView: 3,
//     loop: true,
//     breakpoints: {
//         768: {
//             slidesPerView: 2
//         },
//         480: {
//             slidesPerView: 2
//         },
//     }
// });

function setRating() {
    if ($('.reviews__rating:not(.init)').length) {
        $('.reviews__rating:not(.init)').each(function () {
            var ratingInt = Math.floor($(this).data('rating'));
            var ratingDec = $(this).data('rating') - ratingInt;
            $(this).html(`<span>${ratingInt}.0 </span>`);
            for (var i = 0; i < ratingInt; i++) {
                $(this).prepend('<i class="fas fa-star"></i>');
            }

            if (ratingDec) {
                if (ratingDec > 0 && ratingDec <= 0.5) {
                    $(this).append('<i class="fas fa-star-half"></i>');
                } else if (ratingDec > 0 && ratingDec > 0.5) {
                    $(this).append('<i class="fas fa-star"></i>');
                }
            }
            $(this).addClass('init');
            reviewSwiper.update();
        });
    }
}

setRating();
