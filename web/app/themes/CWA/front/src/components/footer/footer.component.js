import './footer.component.scss';
import './footer.component.pug';
import Swiper from 'swiper';

import Snackbar from 'node-snackbar';

document.addEventListener('wpcf7submit', function(event) {
    let color = '#d84b37';
    if (event.detail.apiResponse.status === 'mail_sent') {
        color = '#43a047';
    }
    Snackbar.show({
        text: event.detail.apiResponse.message,
        pos: 'bottom-center',
        backgroundColor: color,
        textColor: '#ffffff',
        showAction: false,
        actionText: 'Close',
        duration: 5000,
        customClass: 'snackbar-theme-1'
    });

}, false );
// function setRating() {
//     if ($('.reviews__rating:not(.init)').length) {
//         $('.reviews__rating:not(.init)').each(function () {
//             var ratingInt = Math.floor($(this).data('rating'));
//             var ratingDec = $(this).data('rating') - ratingInt;
//             $(this).html(`<span>${ratingInt}.0 </span>`);
//             for (var i = 0; i < ratingInt; i++) {
//                 $(this).prepend('<i class="fas fa-star"></i>');
//             }

//             if (ratingDec) {
//                 if (ratingDec > 0 && ratingDec <= 0.5) {
//                     $(this).append('<i class="fas fa-star-half"></i>');
//                 } else if (ratingDec > 0 && ratingDec > 0.5) {
//                     $(this).append('<i class="fas fa-star"></i>');
//                 }
//             }
//             $(this).addClass('init');
//             // reviewSwiper.update();
//         });
//     }
// }

// setRating();
