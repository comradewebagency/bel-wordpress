import "./practic-description.component.scss";
import "./practic-description.component.pug";
import "../../helpers/styles/base.scss";
import "../../components/header/header.component";
import "../../components/hero/hero.component";
import "../../components/lazy-load/lazy-load.component";
import "../../components/contact-us-block/contact-us-block.component";
import "../../components/footer/footer.component";
import Paralax from "../../../node_modules/jquery-parallax.js";
