import "./top-banner.component.scss";
import "./top-banner.component.pug";
import { positionTopElements } from "../header/header.component";

const topBanner = $(".top-banner");
const topBannerClose = $(".top-banner__close");
topBannerClose.click(() => {
  topBanner.remove();
  localStorage.setItem("topBannerHidden", "1");
  setTimeout(positionTopElements, 50);
});
if (localStorage.getItem("topBannerHidden") === "1") {
  topBanner.remove();
  setTimeout(positionTopElements, 50);
} else {
  topBanner.addClass("active");
}
