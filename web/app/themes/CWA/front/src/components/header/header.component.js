import "./header.component.scss";
import "./header.component.pug";
import "../top-banner/top-banner.component";
import Cleave from "cleave.js";
import Lazy from "jquery-lazy";
// import lozad from "lozad";

// var el = document.querySelectorAll("img");
// var observer = lozad(el);
// observer.observe();
// window.addEventListener("load", function () {
//   var observer = lozad();
//   observer.observe();
// });

$("img[data-src]").Lazy();
$("source[data-src]").Lazy();

const bodyScrollLock = require("body-scroll-lock");
const disableBodyScroll = bodyScrollLock.disableBodyScroll;
const enableBodyScroll = bodyScrollLock.enableBodyScroll;
const bodyScrollElement = document.querySelector(".mobile-menu__nav");

var tel = $("input[type=tel]");
window.scrollOffset = null;
export function positionTopElements() {
  const $stickyElements = $(".social, .attorneys-menu");
  const $topBanner = $(".top-banner");
  const $header = $(".header-inner");
  const $headerIndex = $(".header");
  const $adminBar = $("#wpadminbar");
  const $hero = $(".hero-js");
  const $menu = $(".menu");
  const $heroInner = $(".hero__inner");
  const adminBarOffset = $adminBar.length > 0 ? $adminBar.outerHeight() : 0;
  const topBannerOffset = $topBanner.length > 0 ? $topBanner.outerHeight() : 0;
  const headerHeight =
    $headerIndex.length > 0
      ? $headerIndex.outerHeight()
      : $header.outerHeight();

  window.scrollOffset = adminBarOffset + topBannerOffset + headerHeight;

  if (window.innerWidth > 991) {
    if ($topBanner.length > 0) {
      $topBanner.css("top", `${adminBarOffset}px`);
    }
    if ($stickyElements.length > 0) {
      $stickyElements.css(
        "top",
        `${adminBarOffset + topBannerOffset + headerHeight + 24}px`
      );
    }
    if ($header.length > 0) {
      $header.css("top", `${adminBarOffset + topBannerOffset}px`);
    }
    if ($hero.length > 0) {
      $hero.css("margin-top", `${topBannerOffset + headerHeight}px`);
    }
    $menu.css("top", "");
    if (window.location.pathname === "/") {
      $heroInner.css("padding-top", "");
      if ($headerIndex.hasClass("fixed")) {
        $headerIndex.css("top", `${adminBarOffset + topBannerOffset}px`);
      } else {
        $headerIndex.css("top", "");
      }
    }
    if (window.location.pathname === "/" && $topBanner.length > 0) {
      $(".hero").css("padding-top", `${topBannerOffset}px`);
    } else if (window.location.pathname === "/") {
      $(".hero").css("padding-top", "");
    }
    return false;
  } else {
    $(".hero").css("margin-top", ``);
  }

  let offset = 0;
  if ($topBanner.length > 0) {
    offset += $topBanner.outerHeight();
    $topBanner.css("top", `${adminBarOffset}px`);
  }
  if ($header.length > 0) {
    offset += $header.outerHeight();
    $header.css("top", `${topBannerOffset + adminBarOffset}px`);
    $hero.css("margin-top", `${offset}px`);
  }
  if ($headerIndex.length > 0) {
    offset += $headerIndex.outerHeight();
    $headerIndex.css("top", `${topBannerOffset + adminBarOffset}px`);
    $heroInner.css("padding-top", `${offset}px`);
  }

  const menuOffset = topBannerOffset + adminBarOffset + headerHeight;
  $menu.css("top", `${menuOffset}px`);
}

$(document).ready(() => {
  window.addEventListener("resize", positionTopElements);
  window.addEventListener("scroll", positionTopElements);
  positionTopElements();
});

function stickyHeader() {
  var n = $(".hero").height() + $(".iconbox").height() + 100;
  var header = $(".header").innerHeight();
  if ($(document).scrollTop() > n) {
    if ($(window).outerWidth() >= 991) {
      $("header").addClass("fixed");
      // debugger;
      if ($(".header").children(".fixed-wrap").length >= 1) {
        return false;
      } else if ($(".header").children(".fixed-wrap").length) {
        return false;
      } else {
        $(".header .header__inner, .header .menu").wrapAll(
          '<div class="fixed-wrap"></div>'
        );
      }
    } else {
      $("header").addClass("fixed-mobile").removeClass("fixed");
    }
  } else if ($(window).outerWidth() <= 991) {
    $("header").addClass("fixed-mobile");
    $(".hero__inner").css("padding-top", header + "px");
  } else {
    if ($(window).outerWidth() >= 991) {
      $("header").removeClass("fixed");
      $(".header__inner, .menu").unwrap(".fixed-wrap");
      if ($("header").hasClass("fixed-mobile")) {
        $("header").removeClass("fixed-mobile");
      }
    } else {
      $("header").removeClass("fixed-mobile");
    }
  }
}
$(window).scroll(function () {
  stickyHeader();
});
$(window).resize(function () {
  stickyHeader();
});
$(window).scroll(function () {
  stickyHeader();
});
$(window).resize(function () {
  stickyHeader();
});

stickyHeader();

// $(".header-desktop .action-menu").click((function() {
//     $(".header-desktop").hasClass("open") ? ($(".header-desktop").removeClass("open"),
//     $(".container-nav").removeClass("open")) : ($(".header-desktop").addClass("open"),
//     setTimeout((function() {
//         $(".container-nav").addClass("open")
//     }
//     ), 50))
// }

$(".hamburger").click(function () {
  if (!$("body").hasClass("open-menu")) {
    $("body").addClass("open-menu");
    $(".menu.menu-h0").addClass("open");
    $(".hamburger").addClass("hamburger-open");
    disableBodyScroll(bodyScrollElement);
  } else {
    $(".menu.menu-h0").removeClass("open");
    $(".hamburger").removeClass("hamburger-open");
    $("body").removeClass("open-menu");
    enableBodyScroll(bodyScrollElement);
  }
});
function hoverLink() {
  $(".header-nav__link").hover(function (e) {
    $(e.target).siblings(".header-nav__link").css("opacity", ".5");
  });
}
var menu = $(".open-menu .menu");
menu.on("hover", function () {});

$(".menu-h0 .header__nav-link span").mouseleave(function (e) {
  var parentLink = $(e.target).closest("li");
  $(parentLink).siblings("li").css("opacity", "1");
});
$(".menu-h0 .header__nav-link span").mouseenter(function (e) {
  var parentLink = $(e.target).closest("li");
  $(parentLink).siblings("li").css("opacity", ".5");
});

var wpcf7Elm = document.querySelector(".contact-us__form");

$(".contact-us__form").submit(function () {
  $(this).siblings(".preloader").addClass("active");
});

if (wpcf7Elm) {
  wpcf7Elm.addEventListener(
    "wpcf7-submit",
    function () {
      $(this).siblings(".preloader").removeClass("active");
    },
    false
  );
}

$(".header__arrow, .hero__arrow").click(function (e) {
  var n = $(".hero").height() + $(".iconbox").height() + window.scrollOffset;
  e.preventDefault();
  $("html, body").animate(
    {
      scrollTop: n,
    },
    1000,
    "swing"
  );
});
$(".hero__arrow").click(function (e) {
  var n = $(".hero").height() + $(".iconbox").height() - 30;
  e.preventDefault();
  $("html, body").animate(
    {
      scrollTop: n,
    },
    1000,
    "swing"
  );
});
if (tel.length) {
  tel.each(function (i) {
    new Cleave(tel[i], {
      blocks: [3, 3, 4],
      delimiters: [" ", "-", "-"],
      numericOnly: true,
    });
  });
}

const crossScroll = {
  scrollTo(target) {
    if (typeof target === "string") {
      target = document.querySelector(target);
    }
    if (target) {
      $("html, body")
        .stop()
        .animate(
          {
            scrollTop: $(target).offset().top - window.scrollOffset - 24,
          },
          400
        );
      if (target.classList.contains("case-studies__item")) {
        target.classList.add("case-studies__item--js");
      }
      return true;
    } else {
      console.warn("Scroll target:", target);
      return false;
    }
  },
  scrollHandler(event) {
    const target = event.target.closest("a").dataset.scrollToQuery;
    const isCurrent = this.scrollTo(target);
    if (isCurrent) {
      event.preventDefault();
    } else if (target) {
      localStorage.setItem("scroll-target", target);
    }
  },
  init() {
    document.querySelectorAll("[data-scroll-to-query]").forEach((link) => {
      const newUrl = link.href.split("#")[0];
      if (newUrl) {
        link.href = newUrl;
      }
      link.addEventListener(
        "click",
        crossScroll.scrollHandler.bind(crossScroll)
      );
    });
    $(document).ready(() => {
      const target = localStorage.getItem("scroll-target");
      if (target) {
        crossScroll.scrollTo(target);
        localStorage.removeItem("scroll-target");
      }
    });
  },
};
crossScroll.init();
