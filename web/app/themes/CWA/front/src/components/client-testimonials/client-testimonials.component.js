import "./client-testimonials.component.scss";
import "./client-testimonials.component.pug";
// import Masonry from "../../../node_modules/masonry-layout/masonry";

// var elem = document.querySelector(".client-testimonials__row ");
// var msnry = new Masonry(elem, {
//   horizontalOrder: true,
//   isResizable: true,
//   singleMode: true,
//   gutter: 6,
//   itemSelector: ".client-testimonials__item",
//   percentPosition: true,
//   transitionDuration: "0.8s",
// });
function setRating() {
  if ($(".reviews__rating:not(.init)").length) {
    $(".reviews__rating:not(.init)").each(function () {
      var ratingInt = Math.floor($(this).data("rating"));
      var ratingDec = $(this).data("rating") - ratingInt;
      $(this).html(`<span>${ratingInt}.0 </span>`);
      for (var i = 0; i < ratingInt; i++) {
        $(this).prepend('<i class="fas fa-star"></i>');
      }

      if (ratingDec) {
        if (ratingDec > 0 && ratingDec <= 0.5) {
          $(this).append('<i class="fas fa-star-half"></i>');
        } else if (ratingDec > 0 && ratingDec > 0.5) {
          $(this).append('<i class="fas fa-star"></i>');
        }
      }
      $(this).addClass("init");
      // reviewSwiper.update();s
    });
  }
}

setRating();
