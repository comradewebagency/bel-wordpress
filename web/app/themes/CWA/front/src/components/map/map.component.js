import './map.component.scss'
import './map.component.pug'

import {loader} from './map-api-loader'

var maps = $('.map-block')

if (maps) {
  loadMap(maps)

  function loadMap(maps) {
    var urlmap = maps[0].dataset.urlmap
    loader.key = maps[0].dataset.key
    loader().then(function (googleMaps) {
      maps.each(function (i) {
        var isVisibleMap = function (target) {
          var targetPosition = {
            top: window.pageYOffset + target.getBoundingClientRect().top,
            left: window.pageXOffset + target.getBoundingClientRect().left,
            right: window.pageXOffset + target.getBoundingClientRect().right,
            bottom: window.pageYOffset + target.getBoundingClientRect().bottom
          }

          var windowPosition = {
            top: window.pageYOffset,
            left: window.pageXOffset,
            right: window.pageXOffset + document.documentElement.clientWidth,
            bottom: window.pageYOffset + document.documentElement.clientHeight
          }

          if (targetPosition.bottom > windowPosition.top &&
              targetPosition.top < windowPosition.bottom &&
              targetPosition.right > windowPosition.left &&
              targetPosition.left < windowPosition.right) {

            if (!target.classList.contains('load')) {
              loader.language = "en"

              var lat = +target.dataset.lat
              var lng = +target.dataset.lng
              var center = {lat, lng}

              var map = new googleMaps.Map(target, {
                center: {lat, lng},
                disableDefaultUI: true,
                zoom: 16,
                styles: [
                  {
                    "stylers": [
                      {
                        "saturation": -100
                      }
                    ]
                  }
                ]
              })

              window.addEventListener('load', function () {
                if (window.innerWidth >= 1400) {
                  map.panBy(100, 0)
                }
                if (window.innerWidth >= 1280) {
                  map.panBy(250, 0)
                }
                if (window.innerWidth >= 1050) {
                  map.panBy(-300, 50)
                }
                if (window.innerWidth >= 991) {
                  map.panBy(-400, 50)
                }
                if (window.innerWidth <= 991) {
                  map.panBy(0, 0)
                }
                if (window.innerWidth <= 480) {
                  map.panBy(0, 0)
                }
              })

              var icon = {
                path: "M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80",
                fillColor: '#3A6EAA',
                fillOpacity: 1,
                anchor: new google.maps.Point(200, 500),
                strokeWeight: 0,
                scale: 0.07,
                style: {
                  pointerEvents: "auto",
                }

              }

              var marker = new google.maps.Marker({
                position: center,
                icon: icon,
                url: urlmap,
                map
              })
              google.maps.event.addListener(marker, 'click', function () {
                // window.location.href = marker.url;
                console.log(1)
                window.open(marker.url, "_blank")
              })
              target.classList.add('load')
            }
          }
        }


        window.addEventListener('scroll', function () {
          isVisibleMap(maps[i])
        }, false)

        isVisibleMap(maps[i])
      })

    }).catch(function (error) {
      console.error(error)
    })
  }
}
