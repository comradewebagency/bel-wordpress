import './testimonials-block.component.scss';
import './testimonials-block.component.pug';
import Swiper from 'swiper';

let contentSlider = new Swiper('.testimonials-block__swiper-container', {
    spaceBetween: 10,
    slidesPerView: 1,
    // loop: true,
    // loopedSlides: 1, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    // effect: 'fade',
});
let titlesSlider = new Swiper('.testimonials-block__swiper-titles', {
    spaceBetween: 10,
    //   loop:true,
    //   effect: 'fade',
    //   loopedSlides: 1, //looped slides should be the same
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: contentSlider,
      },
      autoplay: {
        delay: 5000,
      },
      breakpoints: {
        768: {
            slidesPerView: 1,
            autoHeight: true
        },
        320: {
            autoHeight: true
        },
    }
});