/* eslint-disable */
'use strict';
const fs = require('fs');
const mkdirp = require('mkdirp');
const blockName = process.argv[2];
const defaultExtensions = ['scss', 'pug', 'js'];

// const extensions = uniqueArray(defaultExtensions.concat(process.argv.slice(3)));
const extensions = uniqueArray(defaultExtensions);

if (blockName) {
  const dirPath = `src/pages/${blockName}/`;
  mkdirp(dirPath, (err) => {
    if (err) {
      console.error(`[CWA] Cancel operation: ${err}`);
    }

    else {
      console.log(`[CWA] Folder creation: ${dirPath} (if absent)`);

      extensions.forEach((extension) => {
        const filePath = `${dirPath + blockName}.${extension}`;
        let fileContent = '';
        let fileCreateMsg = '';

        if (extension === 'scss') {
          fileContent = `@import "~normalize.css/normalize.css";`;
          // fileCreateMsg = '';
        }

        else if (extension === 'js') {
          fileContent = `import './${blockName}.scss';\nimport '../../helpers/styles/base.scss'\nimport './${blockName}.pug';\n// import '../../components/example/example.component';`;
        }

        else if (extension === 'md') {
          fileContent = '';
        }

        else if (extension === 'pug') {
          fileContent = `extends ../../helpers/markup/layout.pug\n//-include ../../components/example/example.component.pug\n\nblock title\n  title ${blockName} title\nblock page-content\n  p ${blockName} page`;
        }

        else if (extension === 'img') {
          const imgFolder = `${dirPath}img/`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`[CWA] Creating a folder: ${imgFolder} (if absent)`);
            });
          } else {
            console.log(`[CWA] (already exists) `);
          }
        }

        else if (extension === 'bg-img') {
          const imgFolder = `${dirPath}bg-img/`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`[CWA] Creating a folder: ${imgFolder} (if absent)`);
            });
          } else {
            console.log(`[CWA] The ${imgFolder} folder is NOT created (already exists) `);
          }
        }

        else if (extension === 'assets') {
          const imgFolder = `${dirPath}assets/img`;
          if (fileExist(imgFolder) === false) {
            mkdirp(imgFolder, (err) => {
              if (err) console.error(err);
              else console.log(`[CWA] Creating a folder: assets (if absent)`);
            });
          } else {
            console.log(`[CWA] The assets folder is NOT created (already exists) `);
          }
        }

        if (fileExist(filePath) === false && extension !== 'assets' && extension !== 'img' && extension !== 'bg-img' && extension !== 'md') {
          fs.writeFile(filePath, fileContent, (err) => {
            if (err) {
              return console.log(`[CWA] File NOT created:: ${err}`);
            }
            console.log(`[CWA] File created: ${filePath}`);
            if (fileCreateMsg) {
              console.warn(fileCreateMsg);
            }
          });
        }
        else if (extension !== 'img' && extension !== 'bg-img' && extension !== 'md') {
          console.log(`[CWA] File NOT created:: ${filePath} (already exists)`);
        }
        else if (extension === 'md') {
          fs.writeFile(`${dirPath}readme.md`, fileContent, (err) => {
            if (err) {
              return console.log(`[CWA] File NOT created:: ${err}`);
            }
            console.log(`[CWA] File created: ${dirPath}readme.md`);
            if (fileCreateMsg) {
              console.warn(fileCreateMsg);
            }
          });
        }
      });

    }
  });
} else {
  console.log('[CWA] Cancel operation: component name not specified');
}



function uniqueArray(arr) {
  const objectTemp = {};
  for (let i = 0; i < arr.length; i++) {
    const str = arr[i];
    objectTemp[str] = true;
  }
  return Object.keys(objectTemp);
}

function fileExist(path) {
  const fs = require('fs');
  try {
    fs.statSync(path);
  } catch (err) {
    return !(err && err.code === 'ENOENT');
  }
}
