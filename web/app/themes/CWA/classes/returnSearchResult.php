<?php

class returnSearchResult {
    public $post;

    public function __construct($result) {
        $this->post = $result;
    }

    public function getResultID() {
        return $this->post->ID;
    }

    public function getPostType() {
        return get_post_type($this->getResultID());
    }

    public function getImageAndContentByPostType($postID, $postType) {
        switch ($postType) {

            case 'perspectives';
                return [
                    'image' => get_post_thumbnail_id($postID),
                    'content' => get_the_excerpt($postID)
                ];
            break;
            case 'practice';
                return [
                    'image' => get_post_thumbnail_id($postID),
                    'content' => get_the_excerpt($postID)
                ];
            break;
            case 'attorneys';
                return [
                    'image' => get_post_thumbnail_id($postID),
                    'content' => get_the_excerpt($postID)
                ];
            break;
            case 'services';
                return [
                    'image' => get_post_thumbnail_id($postID),
                    'content' => get_the_excerpt($postID)
                ];
            break;

        }
    }

    public function html() {
        $imageAndContent = $this->getImageAndContentByPostType($this->getResultID(), $this->getPostType());

        echo '<article class="search-result"  >';

            if($imageAndContent['image']) {
                echo '<a class="search-result__picture" href="'.get_the_permalink($this->getResultID()).'" rel="nofollow">
                       <picture><img src="'.get_the_post_thumbnail_url($this->getResultID()).'"</picture>
                  </a>';
            }
            echo  '<div class="search-result__content">
                        <h3 class="h4 search-result__title"><a style="color: #000; font-size: 24px  "  href="'.get_the_permalink($this->getResultID()).'">'.get_the_title($this->getResultID()).'</a></h3>
                        <p>'.$imageAndContent['content'].'</p>
                        <a style="font-size: 16px" class="linkArrow_animate" href="'.get_the_permalink($this->getResultID()).'" rel="nofollow"><i style="margin-right: 10px; padding-top: 5px;" class="fal fa-long-arrow-right"></i>Learn more</a>
                    </div>';

        echo '</article>';
    }
}