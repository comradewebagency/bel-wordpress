$(function($){
    $('#view-cases').click(function(){
        $(this).text('Loading...');
        // console.log(1);
        // debugger
        var data = {
            'action': 'loadmore3',
            'query': posts,
            'page' : current
        };
        $.ajax({
            url: ajaxurl,
            data:data,
            type:'POST',
            success:function(data){
                if( data ) {
                    $('.case-studies__container').append(data);
                    $('#view-cases').text('Load More Case Studies');
                    current++;
                    if (current == max_pages) $("#view-cases").remove();
                
                } else {
                    $('#view-cases').remove();
                }
            },
            complete: function () {
                window.casesFontSizeCorrect()
            }
        });
    });
});
