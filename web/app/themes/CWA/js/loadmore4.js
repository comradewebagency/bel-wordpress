// import Rateit from rateit;
// import 'https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.3/jquery.rateit.js';

$(function($){
    $('#view-testimonials').click(function(){
        
        $(this).text('Loading...');
        var data = {
            'action': 'loadmore4',
            'query': posts_test,
            'page' : current_test
        };
        $.ajax({
            url: ajaxurl,
            data:data,
            type:'POST',
            success:function(data){
                if( data ) {
                    
                    $('.client-testimonials__row').append(data);
                    $('#view-testimonials').text('Load More Testimonials');
                    current_test++;
                    if (current_test == max_pages) $("#view-testimonials").remove();
                    $('.rateit').rateit();
                    if($('.rateit').hasClass("rateit-font")) {
                        return false;
                    }
                    console.log(dara);
                    // $(".rateit").bind('rated', function() { alert('rating: ' + $(this).rateit('value')); }); 
                } else {
                    $('#view-testimonials').remove();
                }
            },
            error: function(){
                console.log(error);
            }
        });
    });

});
