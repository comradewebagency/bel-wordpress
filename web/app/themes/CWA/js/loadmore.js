

$(function($){
    var msnr = import('https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.js');
    
    $('#true_loadmore').click(function(){
        $(this).text('Loading...');
        // debugger
        var data = {
            'action': 'loadmore',
            'query': true_posts,
            'page' : current_page
        };
        $.ajax({
            url: ajaxurl,
            data:data,
            type:'POST',
            success:function(data){
                if( data ) {
                    $('.content-cards__row-grid').append(data);
                    $('#true_loadmore').text('Load More');
                    current_page++;
                    if (current_page == max_pages) $("#true_loadmore").remove();
                    var el = document.querySelectorAll('img');
                    var observer = lozad(el);
                    observer.observe();
                    window.addEventListener('load', function() {
                        var observer = lozad();
                        observer.observe();
                    })

                } else {
                    $('#true_loadmore').remove();
                }
                
            },
        });
    });

});
