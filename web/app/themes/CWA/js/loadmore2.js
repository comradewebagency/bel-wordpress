

$(function($){
    $('#view-more').click(function(){
        $(this).text('Loading...');
        var data = {
            'action': 'loadmore2',
            'query': true_post,
            'page' : current_pag
        };
        
        $.ajax({
            url: ajaxurl,
            data: data,
            type:'POST',
            success:function(data){
                console.log(data);
                if( data ) {
                    $('.preview-articles__row-item').append(data);
                    $('#view-more').text('Load More');
                    current_pag++;
                    if (current_pag == max_pages) $("#view-more").remove();
                } else {
                    $('#view-more').remove();
                }
            }
        });
    });

});