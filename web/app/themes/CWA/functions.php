<?php
//functions
require_once ('functions/styles-and-scripts.php');
require_once ('functions/acf-option.php');
require_once ('functions/custom-post-type.php');
require_once ('functions/acf-fields.php');
require_once ('functions/theme-function.php');
require_once ('classes/returnSearchResult.php');
require_once ('functions/yoast-seo-custom-vars.php');


function true_load_posts(){

    $args = unserialize( stripslashes( $_POST['query'] ) );
    $args['paged'] = $_POST['page'] + 1;
    $args['post_status'] = 'publish';
    query_posts( $args );

    if( have_posts() ) :
        while( have_posts() ): the_post();
        $default_image = get_field('default_image','option');
        $text_top_block = get_field("text_top"); ?>
        <div class="content-cards__item " style="max-width: 632px" >
                    <?php if (!empty(get_the_post_thumbnail_url())) {?>
                        <a  href="<?php echo get_permalink();?>" rel="nofollow">
                            <picture class="content-cards__img">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                            </picture>
                        </a>
                     <?php } ?>
                <span class="content-cards__item-text">
                <a  href="<?php echo get_permalink(); ?>" class="content-cards__item-title"><?php echo get_the_title(); ?></a>
                    <span class="content-cards__item-subtitle"><?php echo wp_trim_words($text_top_block, 30, "...")?></span>
                    <span class="content-cards__item-date"><?php echo get_the_time('M j, Y') ?></span>
                </span>
                <span class="content-cards__arrow"></span>
            </div>
            <?php endwhile;
    endif;
    die();
}
add_action('wp_ajax_loadmore', 'true_load_posts');

add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');


function true_load_p(){
    $args1 = unserialize( stripslashes( $_POST['query'] ) );
    $args1['paged'] = $_POST['page'] + 1;
    $args1['post_status'] = 'publish';
    query_posts( $args1 );

    if( have_posts() ) :
        while( have_posts() ): the_post();
        ?>

        <div class="preview-articles__item">
                <a class="preview-articles__item-title" href="<?php echo get_permalink()?>"><?php echo get_the_title(); ?></a>
        </div>
        <?php endwhile;  wp_reset_postdata(); wp_reset_query();
    endif;
die();
}
add_action('wp_ajax_loadmore2', 'true_load_p');

add_action('wp_ajax_nopriv_loadmore2', 'true_load_p');


function true_loadmore_cases(){
    $args = unserialize( stripslashes( $_POST['query'] ) );
    $args['paged'] = $_POST['page'] + 1;
    $args['post_status'] = 'publish';
    query_posts($args);
    if( have_posts() ) :
        while( have_posts() ): the_post();
        $default_image = get_field('default_image',"option");
        ?>
         <?php $text = get_field('case_studies_item_inner_text') ?>
        <?php $subtitle = get_field('case_studies_item_text') ?>
        <div class="case-studies__item ">
                            <div class="case-studies__item-face">
                            <?php if (has_post_thumbnail()){?>
                                <picture class="case-studies__item-bg">

                                    <img  src=<?php echo (get_the_post_thumbnail_url()) ?>>
                                </picture>
                                        <?php } else { ?>
                                            <picture class="case-studies__item-bg">
                                                <?php echo picture($default_image);  ?>
                                            </picture>
                                        <?php } ?>
                                <div class="case-studies__item-top">
                                    <picture> <img
                                            src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/long-arrow.svg"
                                            alt="arrows"></picture>
                                </div>
                                <div class="case-studies__item-middle">
                                        <h2 class="h1">
                                            <?php echo the_title() ?>
                                        </h2>
                                    <?php if(!empty($subtitle)) { ?>
                                        <p class="t-uppercase"><?php echo $subtitle ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="case-studies__item-inner">
                                <div class="case-studies__item-content">
                                    <?php if(!empty($text)) { ?>
                                        <p><?php echo  $text ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
        <?php endwhile;  wp_reset_postdata(); wp_reset_query();
    endif;
die();
}

add_action('wp_ajax_loadmore3', 'true_loadmore_cases');
add_action('wp_ajax_nopriv_loadmore3', 'true_loadmore_cases');


function true_loadmore_testimonials(){
    $args = unserialize( stripslashes( $_POST['query'] ) );
    $args['paged'] = $_POST['page'] + 1;
    $args['post_status'] = 'publish';
    query_posts($args);
    if( have_posts() ) : while( have_posts() ): the_post(); $review_rating = get_field("review_rating"); $testimonials_text = get_field("testimonials_text"); ?>
            <div class="client-testimonials__item persent-size ">
                <div class="wrapper-reviews__rating" style="flex-direction: row; align-items: center;">
                    <?php if(!empty( $review_rating)) { ?>
                        <span class="rateit" data-rateit-value="<?php echo $review_rating ?>"  data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font">
                        </span>
                        <span style="margin-left: 10px; font-family: Helvetica;font-style: normal;font-weight: bold;font-size: 14px; line-height: 18px">

                    <?php	if(preg_match("/^\d+$/", $review_rating)) { ?>
                        <?php echo $review_rating.".0" ?>
                    <?php }  else { echo $review_rating; } }?>
                    </span>
                </div>
                    <div class="client-testimonials--content">
                        <?php if(!empty($testimonials_text)) { ?>
                            <?php echo $testimonials_text; ?>
                        <?php } ?>
                    </div>
                    <span class="user uppercase" style="font-family: Helvetica;
font-style: normal;
font-weight: bold;
font-size: 14px;
line-height: 18px">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/ava.svg" alt="User"><span><?php the_title() ?></span>
                        </span>
                </div>
            <?php endwhile;  wp_reset_postdata(); wp_reset_query();
    endif;
die();
}
add_action('wp_ajax_loadmore4', 'true_loadmore_testimonials');
add_action('wp_ajax_nopriv_loadmore4', 'true_loadmore_testimonials');

// add_filter( 'wpcf7-form-control', 'imp_wpcf7_form_elements' );
// function imp_wpcf7_form_elements( $content ) {
//      $str_pos = strpos( $content, 'name="date-from"' );
//     $content = substr_replace( $content, ' autocomplete="both" autocomplete="off" ', $str_pos, 0 );
//     $str_pos = strpos( $content, 'name="date-to"' );
//     $content = substr_replace( $content, ' autocomplete="both" autocomplete="off" ', $str_pos, 0 );
//     return $content;
// }
