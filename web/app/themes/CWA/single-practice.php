<?php
$info_block_class = 'info-block--blue';
get_header();
    get_template_part('blocks/cross-site/block', 'hero');
    get_template_part('blocks/practice-inner/block', 'content');
    get_template_part('blocks/practice-inner/preview', 'posts');
    get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();
