<?php
/**
 * Template for displaying all 404 page
 *
 *
 * @package ComradeWebAgency
 * @since CWA Theme
 */
$error_image =   get_field('default_hero_image','option');
$error_background_block =   get_field('error_bacground','option');
$error_button =   get_field('error_button','option');
$error_text =   get_field('error_text','option');
$background_image_hero_block = get_field('background_image_hero');
$default_image_block =  get_field('default_image','option');
if($error_background_block) { $background = $error_background_block; } else { $background = $default_image_block ; }
get_header();
?>
<style>
    .btn  {
        min-width: 200px;
    }
.hero__404 {
    min-height: auto;
    height: 100%;
    flex-grow: 1;
    overflow: hidden;
    display: flex;
    align-items: center;
}
.container--style {
        min-height: 100%;
        display: flex;
        justify-content: center;
        flex-direction: column;
        position: relative;
        align-items: baseline;
}
</style>
    <?php get_template_part('blocks/cross-site/block', 'hero')?>
    <section  style="margin-bottom: 50px" class="container container--style" >
        <?php if($error_text) {?>
        <div class="hero__text">
            <h2 class='h2' style="margin-bottom: 50px"><?php echo $error_text ?></h2>
        </div>
        <?php } else {?>
        <h2 class='h2'>Page not found...</h2>
        <?php } ?>
        <?php if($error_button ) {
            echo link_primary('btn', $error_button, 'nofollow','','');
        } else {?>
        <h3 class="h3">
            <a class="btn btn--big"  style="margin-bottom: 50px" href="/">GO TO HOMEPAGE</a>
        </h3>
        <?php } ?>
    </div>
</section>


<?php
get_footer();