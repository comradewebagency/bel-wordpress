<?php
    $testimonials_title = get_field("testimonials_title");
    $link_name = get_field('link_name');

    $args = array(
        'post_type' => 'testimonials',
        'posts_per_page' => 6
    );
    $query = new WP_Query($args);
?>
<section class="client-testimonials">
    <div class="container">
        <?php if(!empty($testimonials_title)) { ?>
            <h2 class="h2 text-center"><?php echo $testimonials_title ?></h2>
        <?php } ?>
        <div class="client-testimonials__row ">
            <?php $key = 0; ?>
            <?php if($query->have_posts()) { while($query->have_posts()) : $query->the_post();
                    $key++;
                    $review_rating = get_field("review_rating");
                    $testimonials_text = get_field("testimonials_text"); ?>

            <!-- <?php if ($key == 1 || $key == 4) { ?> -->
                <!-- <div class="client-testimonials__col"> -->
            <!-- <?php } ?> -->
                <div class="client-testimonials__item persent-size ">
                    <div class="wrapper-reviews__rating" style="flex-direction: row; align-items: center;">
                    <?php if(!empty( $review_rating)) { ?>
                        <span class="rateit" data-rateit-value="<?php echo $review_rating ?>"  data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font">
                        </span>
                        <span style="margin-left: 10px; font-family: Helvetica;font-style: normal;font-weight: bold;font-size: 14px; line-height: 18px">

                    <?php	if(preg_match("/^\d+$/", $review_rating)) { ?>
                        <?php echo $review_rating.".0" ?>
                    <?php }  else { echo $review_rating; } }?>
                    </span>
                    </div>
                    <div class="client-testimonials--content">
                        <?php if(!empty($testimonials_text)) { ?>
                            <?php echo $testimonials_text; ?>
                        <?php } ?>
                    </div>
                    <span class="user uppercase" style="font-family: Helvetica; font-style: normal; font-weight: bold; font-size: 14px; line-height: 18px">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/ava.svg" alt="User"><span><?php the_title()?></span>
                        </span>
                </div>
                <!-- <?php if ($key == 3 || $key == 6) { ?>
                    </div>
                <?php } ?> -->
                <?php endwhile; }?>
        </div>
        <?php if (  $query->max_num_pages > 1) : ?>
            <script>
                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                    var posts_test = '<?php echo serialize($query->query_vars); ?>';
                    var current_test = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                    var max_pages = '<?php echo $query->max_num_pages; ?>';
            </script>

        <button class="btn btn--tilda loadmore" id="view-testimonials"><span><?php echo $link_name ?></span><span style="margin-left: 10px;" class="tilda"><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/tilda.svg" alt=""></span></button>
        <?php endif; wp_reset_postdata(); wp_reset_query();?>

    </div>
</section>
