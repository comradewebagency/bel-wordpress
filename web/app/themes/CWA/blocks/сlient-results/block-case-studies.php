<?php
$title = get_field('title');

$link_block = get_field('link_content');
$link = get_field('subtitle');
$args = array(
    'post_type' => 'cases',
    'posts_per_page' => 6,
    'order' => 'DESC',
);
$query = new WP_Query($args);

$default_image = get_field('default_image',"option");
?>

<section class="case-studies case-studies__clients" id="case-studies ">
<div class="container">
    <div class="case-studies__top-content">
        <div class="case-studies__top-content-left">
            <?php if(!empty($title)) { ?>
                <h2><?php echo $title?></h2>
                <?php } ?>
        </div>
        <div class="case-studies__top-content-right">
        <?php if(!empty($link)) { ?>
            <p><?php echo $link?></p>
        <?php } ?>
        </div>
        <div class="case-studies__container">

                    <?php if ($query->have_posts()) { ?>
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <?php $text = get_field('case_studies_item_inner_text') ?>
                        <?php $subtitle = get_field('case_studies_item_text') ?>
                        <div class="case-studies__item" id="case_<?php echo get_the_ID(); ?>">
                            <div class="case-studies__item-face">
                            <?php if (has_post_thumbnail()){?>
                                <picture class="case-studies__item-bg">
                                    <img  data-src=<?php echo (get_the_post_thumbnail_url()) ?>>
                                </picture>
                                        <?php } else { ?>
                                            <picture class="case-studies__item-bg">
                                                <?php echo picture($default_image);  ?>
                                            </picture>
                                        <?php } ?>
                                <div class="case-studies__item-top">
                                    <picture> <img
                                            data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/long-arrow.svg"
                                            alt="arrows"></picture>
                                </div>
                                <div class="case-studies__item-middle">
                                        <h2 class="h1">
                                            <?php echo the_title() ?>
                                        </h2>
                                    <?php if(!empty($subtitle)) { ?>
                                        <p class="t-uppercase"><?php echo $subtitle ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="case-studies__item-inner">
                                <div class="case-studies__item-content">
                                    <?php if(!empty($text)) { ?>
                                        <p><?php echo  $text ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; }?>
            </div>
        </div>
        <?php if (  $query->max_num_pages > 1) : ?>
            <script>
                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                    var posts = '<?php echo serialize($query->query_vars); ?>';
                    var current = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                    var max_pages = '<?php echo $query->max_num_pages; ?>';
            </script>
                <!-- </div> -->
            <button class="btn btn--tilda loadmore" id="view-cases">
                <span><?php echo $link_block ?></span>
            </button>
        <?php endif; wp_reset_postdata(); wp_reset_query();?>
    </div>
    </div>
</section>
