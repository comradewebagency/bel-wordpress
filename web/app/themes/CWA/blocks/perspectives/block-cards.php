<?php wp_reset_postdata(); wp_reset_query();
$args = array(
        "order"=>'ASС',
        'post_type' => 'perspectives',
        'post_status' => 'publish',
        'posts_per_page' => 5,
        'orderby' => 'date',
);
$query = new WP_Query($args);
$link_block = get_field("link");
?>
<section class="content-cards content-cards--perspectives">
    <div class="container">
        <div class="content-cards__row-grid">

            <?php if($query->have_posts()) {  ?>
            <?php $key = 0; ?>
            <?php while($query->have_posts()) : $query->the_post();
            $key++;
            $default_image = get_field('default_image','option');
            if ( get_the_post_thumbnail_url()) { $background =  get_the_post_thumbnail_url() ; } else { $background = $default_image; }
            $text_top_block = get_field("text_top");
            ?>
            <?php if ($key == 1 || $key == 3) { ?>
                <div class="content-cards__grid-col">
            <?php } ?>
            <?php if ($key == 1 ) { ?>
                <div class="content-cards__item content-cards__item-big" >
            <?php } else  { ?>
                <div class="content-cards__item " >
            <?php } ?>
            <?php if (!empty(get_the_post_thumbnail_url())) {?>
                        <a  href="<?php echo get_permalink();?>" rel="nofollow">
                            <picture class="content-cards__img">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                            </picture>
                        </a>
                    <?php }  else { ?>
                        <picture class="content-cards__img">
                        <?php echo picture(get_the_post_thumbnail_url(),'', '', '', 1); ?>
                        </picture>
                    <?php } ?>
            <span class="content-cards__item-text">
            <a href="<?php echo get_permalink( );?>" class="content-cards__item-title"><?php echo get_the_title(); ?></a>
                <span class="content-cards__item-subtitle"><?php echo wp_trim_words($text_top_block, 30, "...")?></span>
                <span class="content-cards__item-date"><?php echo get_the_time('M j, Y') ?></span>
            </span>
            <span class="content-cards__arrow"></span>
                 </div>
                    <?php if ($key == 2 || $key == 5) { ?>
                        </div>
                <?php } ?>
            <?php endwhile; wp_reset_postdata(); wp_reset_query();   }?>
        </div>

        <?php if (  $query->max_num_pages > 1) : ?>
            <script>
                var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                var true_posts = '<?php echo serialize($query->query_vars); ?>';
                var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                var max_pages = '<?php echo $query->max_num_pages; ?>';
            </script>
        <div class="content-cards__row"><button class="btn btn--tilda testimonials-block__btn-bottom loadmore" id="true_loadmore" ><span><?php echo $link_block; ?></span><span style="margin-left: 10px" class="tilda"><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/tilda.svg" alt=""></span></button></div>

    <?php endif; ?>

    </div>
</section>
