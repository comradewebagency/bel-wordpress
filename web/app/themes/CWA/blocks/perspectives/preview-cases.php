<?php
        $args1 = array(
            'post_type' => 'practice',
            'posts_per_page' => 8,
            'order' => 'DESC',
            'post_status' => 'publish',
            'post__not_in' => array(get_the_ID())
        );
        $query = new WP_Query($args1);
        $cases_title = get_field('cases_title');
?>

<section class="preview-articles">
        <div class="container">
            <?php if(!empty($cases_title)) { ?>
                <h2 class="h2 preview-articles__title"><?php echo $cases_title ?></h2>
          <?php } ?>
          <div class="preview-articles__row preview-articles__row-item">
          <?php if($query->have_posts()) { ?>
            <?php while($query->have_posts()) : $query->the_post(); ?>
                <div class="preview-articles__item">
                    <a class="preview-articles__item-title" href="<?php echo get_permalink()?>"><?php echo get_the_title(); ?></a>
                </div>
                <?php endwhile; }?>
          
          
          <?php if (  $query->max_num_pages > 1) : ?>
                <script>
                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                    var true_post = '<?php echo serialize($query->query_vars); ?>';
                    var current_pag = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                    var max_pages = '<?php echo $query->max_num_pages; ?>';
                </script>
            <div class="preview-articles__row"><button class="btn btn--tilda loadmore" id="view-more"><span>View more</span><span style="margin-left: 10px" class="tilda"><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/tilda.svg" alt=""></span></button></div>
            
            <?php endif; wp_reset_postdata(); wp_reset_query();?>
            </div>
    </section>