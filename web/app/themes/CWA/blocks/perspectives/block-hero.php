<?php
    $home_hero_slide_title_block = get_field('hero_title');
    $home_hero_slide_image_block = get_field('hero_image');
    $default_image = get_field('default_image','option');

    if( $home_hero_slide_image_block ) { $background =  $home_hero_slide_image_block ; } else { $background = $default_image; }
    $args = array(
        'post_type' => 'perspectives',
        'posts_per_page' => 9,
        // 'order' => 'DESC',
    );
    $query = new WP_Query($args);

?>
<section class="hero hero-perspectives hero-js">
    <?php if(!empty($background)) { ?>
    <picture class="hero-inner__bg">
        <img data-src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
    </picture>
    <?php } ?>
    <div class="container">
        <div class="hero-inner__row">
            <?php if(!empty($home_hero_slide_title_block)) { ?>
                <h1 class="h2 h2--white"><?php echo $home_hero_slide_title_block; ?></h1>
            <?php } else { ?>
                <h1 class="h2 h2--white"><?php echo the_title(); ?></h1>
            <?php } ?>
            <div class="form-horizontal">
                <form role="search" method="POST" action="<?php echo esc_url( home_url( '/' )); ?>">
                            <div class="input__wrap">
                            <input class="input" value="<?php echo get_search_query() ?>" name="s" id="s"  name="query" type="text" placeholder="Search">
                        </div>
                        <button type="submit"><i class="fal fa-search"></i></button>
                </form>
            </div>
        </div>
</section>
<?php the_breadcrumb(); ?>

