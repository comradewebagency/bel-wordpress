<?php
    $content_bottom = get_field('content_bottom');
    $review_list = get_field('review_list');
    $review_title_top = get_field('review_title_top');
    $default_image = get_field('default_image','option');
    $reviews_link =get_field('reviews_link')
?>

<section class="review-block">
    <div class="container">
        <div class="review-block__row">
            <div class="review-block__col">
                <?php if(!empty( $content_bottom)) { ?>
                    <?php echo $content_bottom ?>
                <?php } ?>
            </div>
            <div class="review-block__col">
              <div class="review-block__slider">
                <?php if(!empty($review_title_top)) { ?>
                    <div class="review-block__slider-title"><?php echo $review_title_top; ?></div>
                <?php }?>
                <div style="display: flex; flex-wrap: wrap; align-items: center; justify-content: space-between; padding-bottom: 20px;">
                    <?php if(!empty($review_list)) { ?>
                      <?php foreach($review_list as $item) { ?>
                            <div class="wrapper-reviews__rating">
                            <?php if(!empty($item['review_image'] )) { ?>
                                <a href="<?php echo $item['reviews_link']['url']?> " target="_blank">
                                <picture class="reviews__rating-img">
                                    
                                        <img src="<?php echo  $item['review_image']['url']?>" alt="<?php echo $item['review_image']["alt"]?>">
                                    
                                </picture>
                                </a>
                                <?php } ?>
                                    <div class="wrapper-reviews__rating" style="flex-direction: row; align-items: center;">
                                    <?php if(!empty( $item['review_rating'])) { ?>
                                        <span class="rateit" data-rateit-value="<?php echo $item['review_rating'] ?>"  data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-mode="font">
                                        </span>
                                        <span style="margin-left: 10px; font-family: Helvetica;font-style: normal;font-weight: bold;font-size: 14px; line-height: 18px">
                                    
                                        <?php	if(preg_match("/^\d+$/", $item['review_rating'])) { ?>
                                            <?php echo $item['review_rating'].".0" ?>
                                        <?php }  else { echo $item['review_rating']; } }?> 
                                    </span>
                                   
                                   
                                    
                            </div>
                            </div>
                      <?php } }?>
                </div>
              </div>
            </div>
          </div>
        </div>
</section>