<?php
    $block_content = get_field('content');
    $block_image = get_field('image');
    $block_image_user = get_field('user');
    $block_title_user = get_field('title');
    $default_image = get_field('default_image','option');
    if( $block_image  ) { $background =  $block_image  ; } else { $background = $default_image; }
    if( $block_image_user  ) { $background1 =  $block_image_user  ; } else { $background1 = $default_image; }
?>

<section class="practic-description practic-description__advanteges">
    <picture class="practic-description__advanteges-bg">
        <img data-src=<?php echo $background['url'] ?> alt="<?php echo $background['alt'] ?>">
    </picture>
    <section class="list-content-block">
        <div class="container">
            <div class="list-content-block__content-row">
                <div class="list-content-block__content-col">
                    <?php if(!empty($block_content)) { ?>
                        <?php echo $block_content; ?>
                    <?php } ?>
                </div>
                <div class="list-content-block__content-col">
                    <div class="description-block">
                        <picture class="description-block__user-img">
                        <img data-src=<?php echo $background1['url'] ?> alt="<?php echo $background1['alt'] ?>">
                        </picture>
                        <?php if(!empty($block_title_user)) { ?>
                            <h3 class="description-block__user-title"><span><?php echo $block_title_user ?></span>
                                <picture><img <img
                                        data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/brackets.svg" alt="img"></picture>
                            </h3>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
