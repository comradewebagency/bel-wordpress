<?php 
    $title = get_field('title');
    $user = get_field('user_image');
    $link_phone = get_field('link_phone');
    $link_email = get_field('link_email');
    $desription = get_field('desription');
    $image = get_field('image');
    $testimonials_list = get_field('testimonials_list');
    $default_image = get_field('default_image','option');
    $title_apply = get_field('title_apply');
    $link_apply = get_field('link_apply');
    $content_apply = get_field('content_apply');
    $more_on_block = get_field('more_on');
    if( $image ) { $background =  $image ; } else { $background = $default_image; }
    if(  $user ) { $background_user =  $user ; } else { $background_user = $default_image; }
?>

<?php get_template_part('blocks/common/block', 'hero'); ?>
<section class="attorneys-inner">
    <?php if(!empty($content_apply)) { ?>
        <picture class="attorneys-inner__bg"><img data-src="<?php echo $background['url']?>" alt=<?php $background['alt']?>>
        </picture>
    <?php } ?>
    <div class="container">
        <div class="attorneys-inner__row">
            <div class="card-user">
                <div class="card-user__row">
                    <div class="card-user__col">
                        <div class="card-user__info">
                            <?php if(!empty($title)) { ?>
                            <div class="card-user__title"><span><?php echo $title ?></span>
                                <?php } ?>
                                <picture><img
                                        src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/brackets.svg"
                                        alt="img"></picture>
                            </div>
                            <?php if(!empty($desription)) { ?>
                                <?php echo $desription ?>
                            <?php } ?>
                            <div class="card-user__info-links">
                                <div class="contact-information">
                                <?php if(!empty($link_phone)) { ?>
                                    <a class="contact-information__item"
                                        href="tel:<?php echo number_phone($link_phone)?>">
                                        <picture><img
                                                src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/phone.svg"
                                                alt="Phone"></picture><span><?php echo  $link_phone?></span>
                                    </a>
                                    <?php } ?>
                                    <?php if(!empty( $link_email)) { ?>
                                        <a class="contact-information__item" href="mailto:<?php echo  $link_email; ?>">
                                            <picture><img
                                                    src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/mail.svg"
                                                    alt="mail"></picture><span><?php echo  $link_email; ?></span>
                                        </a>
                                    <?php } ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-user__col">
                        <picture class="attorneys-inner__user"><img data-src="<?php echo $background_user['url']?>"
                                alt="User"></picture>
                    </div>
                </div>
            </div>
        </div>
        <div class="attorneys-inner__row">
            <div class="attorneys-inner__col">
                <ul class="description-user">
                    <?php if(!empty($testimonials_list)) { ?>
                        <?php foreach ($testimonials_list as $key => $item) {?>
                            <li class="description-user__item" id="<?php echo sanitize_title($item['biography_title']); ?>">
                              <?php if(!empty($item['biography_title'])) { ?>  
                                <h2><?php echo $item['biography_title']?></h2>
                            <?php } ?>
                            <?php if(!empty($item['biography_text'])) { ?>
                                <?php echo $item['biography_text'] ?>
                            <?php } ?>
                            </li>

                    <?php }} ?>
                </ul>
            </div>
            <div class="attorneys-inner__col">
                <div class="attorneys-menu-wrapper">
                    <ul class="attorneys-menu">
                    <?php if(!empty($testimonials_list)) { ?>
                        <?php foreach ($testimonials_list as $key => $item) {?>
                            <li class="attorneys-menu__item attorneys-menu__item-active">
                                <?php if(!empty($item['biography_title'])) { ?>  
                                    
                            <a class="uppercase" href="<?php echo '#'.sanitize_title($item['biography_title']); ?>"
                                    rel="m_PageScroll2id"><?php echo $item['biography_title']?>
                                <?php } ?>    </a></li>
                        <?php } }?>
                    </ul>
                </div>
                <?php if($more_on_block == 1 ) { ?>
                    <?php get_template_part('blocks/cross-site/block', 'infoblock'); ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if(!empty($content_apply)) { ?>
        <div class="attorneys-inner__row--noflef">
            <div class="container">
                <div class="attorneys-inner__row attorneys-inner__row--white">
                    <div class="attorneys-inner__col">
                    
                    <?php if(!empty($title_apply )) { ?>
                        <h2><?php echo $title_apply  ?></h2>
                    <?php } ?>
                    
                    <?php if(!empty($link_apply)) { ?>
                        <a class="link link__icon uppercase" href="<?php echo $link_apply['url']?>">
                            <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon"></picture><span><?php echo $link_apply['title']?></span>
                        </a>
                        <?php } ?>
                    </div>
                    <div class="attorneys-inner__col">
                        <?php if(!empty($content_apply)) { ?>
                            <?php echo $content_apply ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</section>
