<?php
    $iconbox_list_block = get_field("iconbox_list");
?>
<section class="iconbox">
        <div class="iconbox__inner container">
        <?php
            if(!empty($iconbox_list_block)) {
                foreach($iconbox_list_block as $item ) { ?>
                    <div class="iconbox__item">
                        <picture class="iconbox__img">
                            <?php echo picture($item['iconbox_image'],$item['iconbox_image']['alt'],"","" ) ?>
                        </picture>
                        <p class="iconbox__item-text">
                            <?php echo  $item['home_wcu_list_text']?>
                        </p>
                    </div>
        <?php } } ?>
        </div>
</section>
