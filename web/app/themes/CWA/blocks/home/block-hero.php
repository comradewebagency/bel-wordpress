<?php
    $home_hero_slide_subtitle_block = get_field('home_hero_slide_subtitle');
    $home_hero_slide_name_block = get_field('home_hero_slide_name');
    $home_hero_slide_title_block = get_field('home_hero_slide_title');
    $home_hero_slide_link_block = get_field('home_hero_slide_link');
    $home_hero_slide_image_block = get_field('home_hero_slide_image');
    $home_hero_slide_video_block = get_field('home_hero_slide_video');
    $phone_number_block = get_field('phone_number', 'option');
?>

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta content="telephone=yes" name="format-detection">
    <meta name="HandheldFriendly" content="true">
    <meta name="msapplication-TileColor" content="#0E0E0E">
    <meta name="theme-color" content="#0E0E0E">
    <?php wp_head() ?>
    <body>
<script>
    !function (e, n, t) {
        function o(e, n) {
            return typeof e === n
        }

        function s() {
            var e, n, t, s, a, i, r;
            for (var l in c) if (c.hasOwnProperty(l)) {
                if (e = [], n = c[l], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length)) for (t = 0; t < n.options.aliases.length; t++) e.push(n.options.aliases[t].toLowerCase());
                for (s = o(n.fn, "function") ? n.fn() : n.fn, a = 0; a < e.length; a++) i = e[a], r = i.split("."), 1 === r.length ? Modernizr[r[0]] = s : (!Modernizr[r[0]] || Modernizr[r[0]] instanceof Boolean || (Modernizr[r[0]] = new Boolean(Modernizr[r[0]])), Modernizr[r[0]][r[1]] = s), f.push((s ? "" : "no-") + r.join("-"))
            }
        }

        function a(e) {
            var n = u.className, t = Modernizr._config.classPrefix || "";
            if (p && (n = n.baseVal), Modernizr._config.enableJSClass) {
                var o = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
                n = n.replace(o, "$1" + t + "js$2")
            }
            Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), p ? u.className.baseVal = n : u.className = n)
        }

        function i() {
            return "function" != typeof n.createElement ? n.createElement(arguments[0]) : p ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments)
        }

        function r() {
            var e = n.body;
            return e || (e = i(p ? "svg" : "body"), e.fake = !0), e
        }

        function l(e, t, o, s) {
            var a, l, f, c, d = "modernizr", p = i("div"), h = r();
            if (parseInt(o, 10)) for (; o--;) f = i("div"), f.id = s ? s[o] : d + (o + 1), p.appendChild(f);
            return a = i("style"), a.type = "text/css", a.id = "s" + d, (h.fake ? h : p).appendChild(a), h.appendChild(p), a.styleSheet ? a.styleSheet.cssText = e : a.appendChild(n.createTextNode(e)), p.id = d, h.fake && (h.style.background = "", h.style.overflow = "hidden", c = u.style.overflow, u.style.overflow = "hidden", u.appendChild(h)), l = t(p, e), h.fake ? (h.parentNode.removeChild(h), u.style.overflow = c, u.offsetHeight) : p.parentNode.removeChild(p), !!l
        }

        var f = [], c = [], d = {
            _version: "3.6.0",
            _config: {classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0},
            _q: [],
            on: function (e, n) {
                var t = this;
                setTimeout(function () {
                    n(t[e])
                }, 0)
            },
            addTest: function (e, n, t) {
                c.push({name: e, fn: n, options: t})
            },
            addAsyncTest: function (e) {
                c.push({name: null, fn: e})
            }
        }, Modernizr = function () {
        };
        Modernizr.prototype = d, Modernizr = new Modernizr;
        var u = n.documentElement, p = "svg" === u.nodeName.toLowerCase(),
            h = d._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
        d._prefixes = h;
        var m = d.testStyles = l;
        Modernizr.addTest("touchevents", function () {
            var t;
            if ("ontouchstart" in e || e.DocumentTouch && n instanceof DocumentTouch) t = !0; else {
                var o = ["@media (", h.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
                m(o, function (e) {
                    t = 9 === e.offsetTop
                })
            }
            return t
        }), s(), a(f), delete d.addTest, delete d.addAsyncTest;
        for (var v = 0; v < Modernizr._q.length; v++) Modernizr._q[v]();
        e.Modernizr = Modernizr
    }(window, document);

</script>
<main class="main">
<?php $banner_text = get_field('top_banner_text', 'option'); ?>
<?php if($banner_text) { ?>
    <div class="top-banner">
        <?php echo str_replace(['<p>', '</p>'], ['', ''], $banner_text); ?>
        <button class="top-banner__close"><span class="fal fa-times"></span></button>
    </div>
<?php } ?>
<section class="hero">
       <?php get_header(); ?>
        <div class="hero__inner">
          <div class="hero__video-wrap">
              <?php if (!empty($home_hero_slide_video_block)) { ?>
                <video class="hero__video" preload="auto" autoplay="true" loop="true" muted="muted" playsinline>
                    <source data-src="<?php echo  $home_hero_slide_video_block['url'] ?>" src="<?php echo  $home_hero_slide_video_block['url'] ?>" type="video/mp4">
                </video>
              <?php } else { ?>
                    <picture>
                        <img style="width: 100%; height:100%; top: 0; left: 0; position: absolute; object-fit: cover; object-position: center;" src="<?php echo $home_hero_slide_image_block['url'] ?>" alt="<?php echo $home_hero_slide_image_block['alt'] ?>" >
                    </picture>
              <?php } ?>
          </div>
          <div class="hero__inner-wrapper">
            <div class="hero__content">
                <?php if(!empty( $home_hero_slide_name_block)) { ?>
                    <h5 class="h5"><?php echo   $home_hero_slide_name_block; ?></h5>
                <?php } ?>
                <?php if(!empty( $home_hero_slide_title_block)) { ?>
                    <h1 class="h1"><?php echo $home_hero_slide_title_block ?></h1>
                <?php } ?>
                <?php if(!empty($home_hero_slide_subtitle_block)) { ?>

                    <p><?php echo $home_hero_slide_subtitle_block ?></p>
                <?php } ?>
                <?php if(!empty($home_hero_slide_subtitle_block)) { ?>
                    <a class="btn btn--tilda" href="<?php echo $home_hero_slide_link_block['url'] ?>" target="<?php echo $home_hero_slide_link_block['target'] ?>">
                    <span><?php echo $home_hero_slide_link_block['title'] ?></span>
                    </a>
                <?php } ?>



              <div class="hero__arrow">
                <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/mobileArrow.svg" alt="Arrow"></picture>
              </div>
            </div>
            <aside class="hero__aside">
              <div class="personal-cabinet">
              <form class="form-search"  role="search" method="POST" action="<?php echo esc_url( home_url( '/' )); ?>">
                    <input class="input" autocomplete="off" value="<?php echo get_search_query() ?>" name="s" id="s"  type="search" placeholder="Search...">
                    <button type="submit" disabled><i class="fal fa-search"></i></button>
                    </form><a class="phone" href="tel:<?php echo number_phone($phone_number_block)?>"><?php echo $phone_number_block ?></a>
              </div>
            </aside>
          </div>
        </div>
      </section>
