<?php 
$description_image_block = get_field("description_image");
$description_title_image_block = get_field("description_title_image");
$description_title_block = get_field("description_title");
$description_text_top_block = get_field("description_text_top");
$description_text_bottom_block = get_field("description_text_bottom");
$description_link_block = get_field("description_link");

$default_image = get_field('default_image',"option");
if($description_image_block) { $background = $description_image_block; } else { $background = $default_image; }
?>
<section class="description-block">
    <div class="container">
        <div class="description-block__row">
            <div class="description-block__user">
                <?php if(!empty($background)) { ?>
                    <picture class="description-block__user-img">
                        <img data-src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
                    </picture>
                <?php } if(!empty($description_title_image_block)) { ?>
                <h3 class="description-block__user-title">
                <span><?php echo $description_title_image_block; ?></span>
                <picture><img data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/brackets.svg" alt="img"></picture>
            </h3>
                <?php } ?>
                
            </div>
            <div class="description-block__content">
                <div class="description-block__content-row">
                <?php if(!empty($description_title_block)) { ?>   
                    <h2><?php echo $description_title_block ?></h2>
                <?php } ?>
                <?php if(!empty($description_text_top_block))  { ?>
                    <?php echo $description_text_top_block ?>
                <?php } ?>
                </div>
                <hr class="description-block__line">
                <div class="description-block__content-row">
                    <div>
                        <?php if(!empty($description_text_bottom_block)) { ?>
                            <?php echo $description_text_bottom_block ?>
                        <?php } ?>
                    </div>
                    <?php if(!empty($description_link_block)) { ?>
                        <a class="link link__icon" href="<?php echo $description_link_block['url']?>" target="<?php echo $description_link_block['target']?>">
                        <picture>
                            <img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon">
                        </picture><span><?php echo $description_link_block['title']?></span>
                    </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
