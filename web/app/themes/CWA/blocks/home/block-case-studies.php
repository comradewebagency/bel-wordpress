<?php
$title = get_field('studies_title');
$link = get_field('studies_link');
$cases_list_block = get_field('cases_list');

$default_image = get_field('default_image',"option");
?>

<section class="case-studies" id="case-studies">
    <div class="case-studies__top-content">
        <div class="case-studies__top-content-left">
            <?php if(!empty($title)) { ?>
                <h2><?php echo $title ?></h2>
            <?php } ?>
        </div>
        <div class="case-studies__top-content-right">
            <?php if(!empty($link)) { ?>
            <a class="btn btn--tilda" href="<?php echo $link["url"] ?>"
                target="<?php echo $link["target"] ?>">
                <span><?php echo $link["title"] ?></span></a>
            <?php } ?>
            </div>
            <div class="case-studies__container">
                <div class="swiper-container case-studies__container-js">
                    <div class="swiper-wrapper">
                    <?php
                    if(!empty($cases_list_block)) { ?>

<!-- <?php var_dump($cases_list_block );?> -->
                     <?php foreach($cases_list_block as $item) { ?>
                        <?php $text = get_field('case_studies_item_inner_text',$item) ?>
                        <?php $subtitle = get_field('case_studies_item_text', $item) ?>
                        <div class="case-studies__item swiper-slide">
                            <div class="case-studies__item-face">
                                <picture class="case-studies__item-bg"><img  data-src=<?php echo (get_the_post_thumbnail_url($item)) ?>></picture>
                                <div class="case-studies__item-top">

                                    <picture> <img
                                            data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/long-arrow.svg"
                                            alt="arrows"></picture>
                                </div>
                                <div class="case-studies__item-middle">
                                        <h2 class="h1">
                                            <?php echo get_the_title($item) ?>
                                        </h2>
                                    <?php if(!empty($subtitle)) { ?>
                                        <p class="uppercase"><?php echo $subtitle ?></p>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="case-studies__item-inner">
                                <div class="case-studies__item-content">
                                    <?php if(!empty($text)) { ?>
                                        <p><?php echo  $text?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php }  }?>
                    </div>
                </div>
        </div>
    </div>
</section>
