<?php
$consultation_title_block = get_field("consultation_title");
$consultation_text_block = get_field("consultation_text");
$consultation_link_block = get_field("consultation_link");
$practice_title_block = get_field("practice_title");
$practice_tex_block = get_field("practice_text");
$practice_list_block = get_field("practice_list");

$default_image = get_field('default_image',"option");
// $practice_link_block = get_field("practice_link");
$practice_img_block = get_field("practice_img");
if($practice_img_block) { $background = $practice_img_block; } else { $background = $default_image; }
// practice_list_title
// 'practice_list_text
$home_hero_slide_subtitle_block = get_field('home_hero_slide_subtitle');
$args = array(
    'post_type' => 'practice',
    'posts_per_page' => -1,
    'order' => 'DESC',
    'post__not_in' => array(get_the_ID())
);
$query = new WP_Query($args);

?>


<section class="practic-description">
    <?php if(!empty($consultation_title_block )) { ?>
        <h2 class="title-icon text-center">
            <picture class="icon-title">
            <img data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/dark-tilda-bg.svg" alt="Icon"></picture>
            <span><?php echo $consultation_title_block ?></span>
        </h2>
    <?php } ?>
    <div class="container">
        <div class="info-block">
            <div class="info-block__container">
                <div class="info-block__row">
                    <div class="info-block__col">
                        <?php if(!empty($consultation_text_block)) { ?>
                            <?php echo $consultation_text_block ?>
                        <!-- <p>When you need the help of an experienced and knowledgeable attorney to assist you with your
                            financial issue, the Belvedere Legal can help. To schedule a consultation, call us at <a
                                href="tel:+14155135980">415-513-5980 </a>or click Schedule Free Consultation button.</p> -->
                        <?php } ?>
                    </div>
                    <?php if(!empty($consultation_link_block)) { ?>
                        <div class="info-block__col"><a class="btn btn--tilda" href="<?php echo $consultation_link_block['url']?>" target="<?php echo $consultation_link_block['target']?>" ><span><?php echo $consultation_link_block['title']?></span></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <picture class="practic-description__bg">
         <img class="parallax-window" data-parallax="scroll" data-image-src="<?php echo $background['url']?>" src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>" >
    </picture>
    <div class="w-practic-description__bg">
        <section class="list-content-block">
            <div class="container">
                <div class="list-content-block__content">
                    <div class="list-content-block__content-row">
                        <div class="list-content-block__content-col">
                            <?php if(!empty($practice_title_block )) { ?>
                                <h2><?php echo $practice_title_block ?></h2>
                            <?php } ?>
                        </div>
                        <div style="font-weight: bold" class="list-content-block__content-col">
                            <?php if(!empty($practice_tex_block)) { ?>
                                <?php echo $practice_tex_block ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="list-content-block__content-row">
                                <ul class="list-block">
                                    <?php if($query->have_posts()) { ?>
                                        <?php while($query->have_posts()) : $query->the_post(); { ?>
                                            <?php $practice_list_text = get_field("text"); ?>
                                        <li class="list-block__item">
                                            <a href="<?php echo get_permalink() ?>" >
                                                    <h4 class="list-block__item-title"><?php echo the_title() ?></h4>
                                                    <?php if(!empty($practice_list_text)) { ?>
                                                        <p class="list-block__item-text"><?php echo
                                                        wp_trim_words( $practice_list_text, 30, "...")
                                                        ?></p>
                                                        <?php } ?>
                                                <picture><img data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/list-item-arrow.svg" alt="arrow"></picture>
                                            </a>
                                        </li>
                                        <?php } endwhile;   wp_reset_query();  } ?>
                            </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
