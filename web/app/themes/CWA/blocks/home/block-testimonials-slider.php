<?php
    $testimonials_title = get_field("testimonials_title");
    $link_name = get_field('link_name');


    $args = array(
        'post_type' => 'testimonials',
        'posts_per_page' => -1,
        'order' => 'DESC',
    );
    $query = new WP_Query($args);

    // $testimonials_list_block = get_field("testimonials_list");
    $testimonials_image_block = get_field("testimonials_image");
    $default_image = get_field('default_image',"option");
    $why_choose_us_title = get_field('why_choose_us_title');
    $why_choose_us_text = get_field('why_choose_us_text');
    $why_choose_us_link = get_field('why_choose_us_link');
    if($testimonials_image_block) { $background = $testimonials_image_block; } else { $background = $default_image; }
?>

<section class="testimonials-block">
    <div class="testimonials-block__slider">
        <div class="container">
            <div class="testimonials-block__row">
                    <div class="testimonials-block__col">
                        <div class="testimonials-block__swiper-titles swiper-container">
                            <div class="swiper-wrapper">
                            <?php if($query->have_posts()) { while($query->have_posts()) : $query->the_post();
                                $title_test_block = get_field('testimonials_title'); ?>

                                <div class="swiper-slide">
                                    <?php if(!empty($title_test_block)) { ?>
                                        <h2 class="description-block__user-title">
                                            <span><?php echo  wp_trim_words($title_test_block , 15, "...") ?></span>
                                            <picture><img
                                                    data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/dark-tilda.svg"
                                                    alt="img"></picture>
                                        </h2>
                                    <?php } ?>
                                </div>
                            <?php endwhile; } ?>
                        </div>
                        </div>
                        <div class="testimonials-block-swiper-navigation">
                            <div class="swiper-button-next swiper-arrow-left"><img
                                    data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/swiper-arrow-left.svg"
                                    alt=""></div>
                            <div class="swiper-button-prev swiper-arrow-right"><img
                                    data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/swiper-arrow-right.svg"
                                    alt=""></div>
                        </div>

                     </div>

                    <div class="testimonials-block__col">
                        <div class="overlay-slider"></div>
                        <div class="testimonials-block__swiper-container swiper-container">
                            <div class="swiper-wrapper">
                            <?php if($query->have_posts()) { while($query->have_posts()) : $query->the_post();
                                 $testimonials_text = get_field("testimonials_text");  ?>
                                <div class="swiper-slide">
                                    <div class="testimonials-block__content">
                                        <div class="testimonials-block__content-text">
                                            <p><?php  echo wp_trim_words( $testimonials_text , 50, "...") ?>
                                        </div>

                                    </div>
                                    <div class="testimonials-block__content-author"><span class="user upercase">
                                                <img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/ava.svg"
                                                    alt="User"></span><span><b><?php the_title()?></b></span>
                                        </div>
                                </div>
                                <?php endwhile; } ?>
                            </div>
                        </div>
                        <div class="testimonials-block-swiper-navigation">
                            <div class="swiper-button-next swiper-arrow-left"><img
                                    data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/swiper-arrow-left.svg"
                                    alt=""></div>
                            <div class="swiper-button-prev swiper-arrow-right"><img
                                    data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/swiper-arrow-right.svg"
                                    alt="">
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <picture class="testimonials-block_bg"><img data-src="<?php echo $background['url'] ?>" alt="<?php echo $background['alt'] ?>"></picture>
 <div class="container">
        <div class="why-choose-us-block">
            <?php if(!empty($why_choose_us_title)) { ?>
                <h2 style="margin-bottom: 0"><?php echo $why_choose_us_title ?></h2>
            <?php } ?>
            <div class="why-choose-us-block__row">
                <div class="why-choose-us-block__col">
                <?php if(!empty($why_choose_us_text)) { ?>
                    <h2><?php echo $why_choose_us_text ?></h2>
            <?php } ?>
            <?php if(!empty($why_choose_us_link)) { ?>
                <a class="btn btn--tilda" href="<?php echo $why_choose_us_link['url']?>" target="<?php echo $why_choose_us_link['target']?>"><span>
                <?php echo $why_choose_us_link['title']?></span></a>
            <?php } ?>
                </div>
            </div>
        </div>

    </div>

</section>
<?php if(!empty($why_choose_us_link)) { ?>
    <a class="btn btn--tilda testimonials-block__btn-bottom" href="<?php echo $why_choose_us_link['url']?>" target="<?php echo $why_choose_us_link['target']?>"><span>
    <?php echo $why_choose_us_link['title']?></span></a>
<?php } ?>
