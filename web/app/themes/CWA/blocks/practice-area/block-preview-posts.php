<?php
$preview_title_block = get_field('preview_title');
$args = array(
    'post_type' => 'practice',
    'posts_per_page' => -1,
    'post__not_in' => array(get_the_ID())
);
$query = new WP_Query($args);
$home_hero_slide_subtitle_block = get_field('home_hero_slide_subtitle');

$title = get_field('title');
$text = get_field('text');
$link = get_field('link');
$practice_bg_top = get_field('practice_bg_top');
$practice_bg = get_field('practice_bg');
// $home_hero_slide_subtitle_block = get_field('home_hero_slide_subtitle');
$default_image = get_field('default_image',"option");
$practice_img_block = get_field("practice_bg");
if($practice_bg_top) { $background = $practice_bg_top; } else { $background = $default_image; }
if($practice_bg) { $background1 = $practice_bg; } else { $background1 = $default_image; }
?>
<section class="practic-description practic-description__practice-area">
    <picture class="practic-description__bg"> <img data-src="<?php echo $background['url']?>"
            alt="<?php echo $background['alt']?>"></picture>
    <section class="list-content-block">
        <div class="container">
            <div class="list-content-block__content-row">
            <div class="list-content-block__content-row">
                    <ul class="list-block">
                        <?php if($query->have_posts()) { ?>
                            <?php while($query->have_posts()) : $query->the_post(); ?>
                            <?php $practice_list_text = get_field("text"); ?>
                             <li class="list-block__item">
                            <a href="<?php echo get_permalink(); ?>">
                                <h4 class="list-block__item-title"><?php the_title();?></h4>
                                <?php if(!empty($practice_list_text)) { ?>
                                                        <p class="list-block__item-text"><?php echo
                                                        wp_trim_words( $practice_list_text, 30, "...")
                                                        ?></p>
                                            <?php } ?>
                                <picture><img
                                        data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/list-item-arrow.svg"
                                        alt="arrow"></picture>
                            </a>
                        </li>
                        <?php endwhile; wp_reset_postdata(); wp_reset_query(); } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="practic-description__wrap-infoblock container">
        <div class="info-block">
            <div class="info-block__container">
                <div class="info-block__row">
                    <div class="info-block__col">
                        <?php if(!empty($title )) { ?>
                        <h3><?php echo $title ?></h3><?php }?>
                        <?php if(!empty($text)) { ?>
                        <?php echo $text ?>
                        <?php } ?>
                        <!-- <p>When you need the help of an experienced and knowledgeable attorney to assist you with your financial issue, the Belvedere Legal can help. To schedule a consultation, call us at <a href="tel:+14155135980">415-513-5980 </a>or click Schedule Free Consultation button.</p> -->

                    </div>
                    <div class="info-block__col">
                        <?php if(!empty($link)) { ?>
                        <a class="btn btn--tilda" href="<?php echo $link['url']?>"
                            target="<?php echo $link['terget']?> "><span><?php echo $link['title']?> </span>
                        </a></div>
                            <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <picture class="practic-description__bg"> <img data-src="<?php echo $background1['url']?>"
            alt="<?php echo $background1['alt']?>"></picture>
</section>
