<?php
$testimonials_list_block = get_field("testimonials_list");

$args = array(
    'post_type' => 'attorneys',
    'posts_per_page' => -1,
    'order' => 'ASC',
);
$query = new WP_Query($args);
$default_image = get_field('default_image','option');
$bio_text = get_field('bio_text');
$bio_text2 = get_field('bio_texts');
?>

<section class="attorneys">
    <div class="container">
        <div class="attorneys__row">

        <?php if($query->have_posts()) {
                    $key = 0;
                    while($query->have_posts()) : $query->the_post();
                        $default_image = get_field('default_image','option');
                        $user = get_field('user_image');
                        $work = get_field('testimonials_work');
                        $testimonials_list = get_field('testimonials_list');
                        $link_phone = get_field('link_phone');
                        $link_email = get_field('link_email');
                        $desription = get_field('desription');

                        if( $user) { $background =  $user; } else { $background = $default_image; }
                        $key++ ?>
                    <?php if($key%2) { ?>
                        <div class="attorneys__item attorneys__item-left">
                                    <a href="<?php echo get_permalink() ?>" rel="nofollow">
                                        <picture class="attorneys__item-user"><img data-src="<?php echo  $background['url']?>" alt=""></picture>
                                    </a>
                                    <div class="attorneys__item-content">
                                        <h2><a href="<?php echo get_permalink() ?>" class="h2"><?php echo the_title() ?></a></h2>
                                        <?php if(!empty($work)) { ?>
                                            <div class="subtitle"><?php echo $work ?></div>
                                        <?php } ?>
                                        <?php if(!empty($desription)) {  ?>
                                                <p><?php echo  $desription ?></p>
                                                <!-- <p><?php echo  wp_trim_words($testimonials_list[0]['biography_text'], 33, "") ?></p> -->
                                        <?php } ?>

                                        <?php if(!empty($link_phone)) { ?>
                                            <a class="link link__icon uppercase" href="<?php echo get_permalink() ?>" rel="nofollow">
                                                <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon" ></picture><span><?php echo $bio_text ?></span>
                                            </a>
                                        <?php } ?>
                                        <?php if(!empty($link_email)) { ?>
                                            <a class="link link__icon uppercase" href='mailto:<?php echo $link_email?>'>
                                                <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon"></picture><span><?php echo $bio_text2 ?> <?php echo the_title() ?></span>
                                            </a>
                                        <?php } ?>
                            </div>
                    </div>
                    <?php } else {  ?>
                        <div class="attorneys__item attorneys__item-right">
                            <a href="<?php echo get_permalink() ?>" rel="nofollow">
                                <picture class="attorneys__item-user"><img data-src="<?php echo $background['url']?>" alt=""></picture>
                            </a>
                                <div class="attorneys__item-content">
                                <h2><a href="<?php echo get_permalink() ?>" class="h2"><?php echo the_title() ?></a></h2>
                                        <?php if(!empty($work)) { ?>
                                            <div class="subtitle"><?php echo $work ?></div>
                                        <?php } ?>
                                        <?php if(!empty($desription)) {  ?>
                                                <p><?php echo  $desription ?></p>
                                        <?php } ?>

                                        <?php if(!empty($link_phone)) { ?>
                                            <a class="link link__icon uppercase" href="<?php echo get_permalink() ?>" rel="nofollow">
                                                <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon" ></picture><span><?php echo $bio_text  ?></span>
                                            </a>
                                        <?php } ?>
                                        <?php if(!empty($link_email)) { ?>
                                            <a class="link link__icon uppercase" href="mailto:<?php echo $link_email; ?>">
                                                <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon"></picture><span><?php echo $bio_text2 ?> <?php echo the_title() ?></span>
                                            </a>
                                        <?php } ?>
                            </div>
                        </div>
                <?php  } endwhile; wp_reset_postdata(); wp_reset_query();  }?>
        </div>
    </div>
</section>
