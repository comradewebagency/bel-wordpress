<?php
$text_block = get_field('text');
$cases_title_block = get_field('cases_title','option');
$cases_link_block = get_field('cases_link','option');
$practice_list_block = get_field('practice_list');

// $args = array(
//     'post_type' => 'cases',
//     'posts_per_page' => 2,
//     'order' => 'DESC',
// );
// $query = new WP_Query($args);
// var_dump($practice_list_block)
?>

<section class="content-block">
    <div class="container">
        <div class="content-block__row">
            <div class="content-block__col">
                <?php if(!empty($text_block)) { echo $text_block ?><?php } ?>
            </div>
            <div class="content-block__col">
                <?php if($practice_list_block) { ?>
                <div class="preview-cases">
                    <?php if(!empty($cases_title_block)) { ?>
                        <div class="preview-cases__title"><?php echo $cases_title_block ?> </div>
                    <?php } ?>
                    <?php if(!empty($practice_list_block )) { ?>
                    <?php foreach($practice_list_block as $item) { ?>
                        <?php
                            $subtitle = get_field('case_studies_item_text');
                            $default_link = get_field('cases_main_page', 'option') ? get_field('cases_main_page', 'option') : '/about/';
                        ?>

                            <a data-scroll-to-query="#case_<?php echo $item->ID ?>" class="preview-cases__item" href=<?php echo $default_link ?>>
                                <h2 class="preview-cases__item-title"> <?php echo get_the_title($item->ID) ?></h2>
                                    <h5 class="h5"><?php echo $item->case_studies_item_text?></h5>
                            </a>
                    <?php }} ?>
                   <?php if(!empty($cases_link_block)) { ?>
                        <a class="link link__icon t-uppercase" href="<?php echo $cases_link_block['url']?>" target="<?php echo $cases_link_block['target']?>">
                            <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg" alt="Icon"></picture><span><?php echo $cases_link_block['title']?></span>
                    </a>
                   <?php } ?>
                </div>
                <?php } ?>
            </div>
            <div class="content-block__col">
                    <?php get_template_part('blocks/cross-site/block', 'infoblock'); ?>
            </div>
        </div>
    </div>
</section>
