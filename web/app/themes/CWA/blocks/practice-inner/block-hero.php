<?php 
    $home_hero_slide_subtitle_block = get_field('home_hero_slide_subtitle');
    $home_hero_slide_title_block = get_field('home_hero_slide_title');
    $home_hero_slide_image_block = get_field('home_hero_slide_image');
    $default_image = get_field('default_image','option');
    if( $home_hero_slide_image_block ) { $background =  $home_hero_slide_image_block ; } else { $background = $default_image; }
?>
<section class="hero hero-inner hero-js">
        <?php if(!empty($background)) { ?>
            <picture class="hero-inner__bg">
                <img data-src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
            </picture>
        <?php } ?>
        <div class="container">
        <div class="hero-inner__row">
            <?php if(!empty( is_singular('practice'))) { ?>
                <h2 class="h2 h2--white"><?php echo the_title(); ?></h2>
            <?php } ?>
            <?php if(!empty( $home_hero_slide_subtitle_block )) { ?>
                <p class="subtitle"><?php echo  $home_hero_slide_subtitle_block ; ?></p>
            <?php } ?>
        </div>
        </div>
</section>
<?php the_breadcrumb(); ?>