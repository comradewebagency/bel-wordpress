<?php
$preview_title_block = get_field('preview_title','option');
$args = array(
    'post_type' => 'practice',
    'posts_per_page' => 6,
    'order' => 'DESC',
    'post__not_in' => array(get_the_ID())
);
$query = new WP_Query($args);
$home_hero_slide_subtitle_block = get_field('home_hero_slide_subtitle');
$default_image = get_field('default_image',"option");
$practice_img_block = get_field("practice_bg",'option');
$practice_list_block = get_field('practice_list');
if($practice_img_block) { $background = $practice_img_block; } else { $background = $default_image; }
?>
<section class="practic-description practic-description__practice-area practic-description__practice-area-inner">
    <picture class="practic-description__bg">
        <?php echo picture($background) ?>
    </picture>
    <div class="list-content-block">
        <div class="container">
            <div class="list-content-block__content-row">
                <?php if(!empty($preview_title_block)) { ?>
                    <h2><?php echo $preview_title_block ?></h2>
                <?php } ?>
            </div>
            <div class="list-content-block__content-row">

                <ul class="list-block">
                <?php  if( have_posts() ) : while($query->have_posts()) : $query->the_post();
                    $text_block = get_field('text')?>
                    <li class="list-block__item">
                            <a href="<?php echo get_permalink(); ?>">
                                <h4 class="list-block__item-title"><?php echo get_the_title(); ?></h4>

                                <?php if(!empty($text_block)) { ?>
                                    <p class="list-block__item-text"><?php echo  wp_trim_words($text_block, 30, "...")  ?></p>
                                <?php } ?>
                                <picture><img
                                        src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/list-item-arrow.svg"
                                        alt="arrow"></picture>
                            </a>
                        </li>
                        <?php   endwhile;
                        endif;
                    ?>
                    </ul>
                </div>
            </div>
        </div>
        </div>
</section>
