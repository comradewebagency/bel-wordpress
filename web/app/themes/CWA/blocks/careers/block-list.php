<?php 
    $default_image = get_field('default_image','option');
    $title_block = get_field('title_block');
    $image_block = get_field('image');
     $vakansy_link_block = get_field('vakansy_link');
    if( $image_block ) { $background =  $image_block ; } else { $background = $default_image; }
    
    // $args = array(
    //     'post_type' => 'practice',
    //     'posts_per_page' => 4,
    //     'order' => 'DESC',
    //     'post__not_in' => array(get_the_ID())
    // );
    $link_vakansy = get_field("vakansy");
    // $query = new WP_Query($args);
    $home_hero_slide_subtitle_block = get_field('text');
?>
<section class="practic-description practic-description__careers">
        
    <picture class="practic-description__careers-bg"><img data-src="<?php echo $background['url'] ?>" alt="<?php echo $background['alt'] ?>"></picture>
    <div class="list-content-block list-content-block--careers">
          <div class="container">
            <div class="list-content-block__content-row">
                <?php if(!empty($title_block)) { ?>
                    <h2><?php echo $title_block ?></h2>
                <?php } ?>
                </div>
                <div class="list-content-block__content-row">
                    <ul class="list-block">
                        <?php if(!empty($link_vakansy)) { ?>
                            <?php foreach($link_vakansy as $item) { ?>

                             <li class="list-block__item">
                            <a href="mailto:<?php echo $vakansy_link_block; ?>?subject=<?php echo $item["vakansy_title"]?>">
                            <?php if(!empty($item["vakansy_title"])) { ?>
                                <h4 class="list-block__item-title"><?php echo $item["vakansy_title"]?></h4>
                            <?php } ?>
                                <?php if(!empty($item['description_vakancy'])) { ?>
                                    <p><?php echo  wp_trim_words($item['description_vakancy'], 30, "...")  ?></p>
                                <?php } ?>
                                <picture><img
                                        data-src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/list-item-arrow.svg"
                                        alt="arrow"></picture>
                            </a>
                        </li>
                        <?php }}?>
                    </ul>
                </div>
                </div>
               
            </div>
          </div>
    </div>
</section>