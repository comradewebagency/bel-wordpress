<?php
    $content_block = get_field('content');
    
    $info_title_block = get_field('info_title');
    $info_content_block = get_field('info_content');
    $link_vakansy = get_field("vakansy");
    // vakansy_link
?>
<section class="careers-block">
        <div class="container">
          <div class="careers-block__row">
            <div class="careers-block__col">
                <?php if(!empty($content_block)) { ?>
                        <?php echo $content_block ?>
                <?php } ?>
                <!-- <?php if(!empty($link_vakansy )) {  foreach( $link_vakansy  as $val) { ?>
                    <?php $vakansy_link_block = get_field('vakansy_link'); ?>
                        <a href="mailto:<?php echo $vakansy_link_block; ?>?subject=<?php echo $val['vakansy_title'] ?>" target="_blank">
                            <?php echo $val['vakansy_title'] ?>
                        </a>
                <?php }}?> -->
              
            </div>
            <div class="careers-block__col">
              <div class="info-block info-block--blue info-block--careers">
                <div class="info-block__container">
                  <div class="info-block__row">
                    <div class="info-block__col">
                        <?php if(!empty($info_content_block)) { ?>
                            <?php echo $info_content_block ?>
                        <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</section>