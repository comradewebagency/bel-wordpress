<?php
    $key_block = get_field("Google_Map_Key",'option');
    $address_map_block = get_field('address-map',"option");
    $Map_link_block =  get_field("google_maps_link" ,"option");
    $contact_title = get_field('contact_title');
    $contact_subtitle = get_field('contact_subtitle');
    $contact_phone_number = get_field('phone_number','option');
    $block_contact_image = get_field('contact_image');
    $default_image = get_field('default_image','option');
    if($block_contact_image) { $background =  $block_contact_image; } else { $background = $default_image; }
?>
<picture class="contact-us__bg">
    <img data-src="<?php echo$background['url'] ?>" alt="<?php echo$background['alt'] ?>">
</picture><section class="w-map-info">
    <?php if(!empty($key_block)) { ?> 
        <div class="map-block" data-key="<?php echo $key_block ?>" data-urlmap="<?php echo $Map_link_block; ?>"  data-lat="<?php echo $address_map_block['lat']?>"  data-lng="<?php echo $address_map_block['lng']?>" ></div>
    <?php } ?>
    
    <!-- <div class="container"> -->
    <div class="map-info">
        <div class="map-info__inner">
            <?php if(!empty($contact_title)) { ?>
                <h3><?php echo $contact_title?></h3>
            <?php } ?>
            <?php if(!empty($contact_subtitle)) { ?>
                <p><?php echo $contact_subtitle?></p>
            <?php } ?>
            <?php if(!empty($contact_phone_number)) { ?>
                <p>Call our office: <a href="tel:<?php echo number_phone($contact_phone_number) ?>"><?php echo $contact_phone_number ?></a></p>
            <?php } ?>
        </div>
    <!-- </div> -->
    </div>
</section>