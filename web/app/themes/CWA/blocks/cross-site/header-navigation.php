<?php
$menuLocations = get_nav_menu_locations();
$menuID = $menuLocations['header_menu'];
$primaryNav = wp_get_menu_array($menuID);
$url = $_SERVER['REQUEST_URI'];
$url_absolute = home_url() . $url;
echo '<nav class="header__nav"><ul>';
foreach ( $primaryNav as $navItem ) {
    if($url == $navItem['url'] || $url_absolute == $navItem['url']) { $class = 'active'; } else { $class = ''; }
    if($navItem['submenu'] != false) {
        echo '<li class="with-submenu">';
        if(!$navItem['url'] || $navItem['url'] == '#') {
            echo '<a class="header__nav-link" title="'.$navItem['title'].'"><span>'.$navItem['title'].'</span><i class="fal fa-angle-down"></i></a>';
        } else {
            echo '<a class="header__nav-link '.$class.'" title="'.$navItem['title'].'" href="'.$navItem['url'].'"><span>'.$navItem['title'].'</span><i class="fal fa-angle-down"></i></a>';
        }
    } else { ;
        
        if ($navItem['title'] == 'Home') { ?> 
            <style>
                @media screen and (min-width: 991px) {
                    .header__nav > ul > li:nth-child(2) {
                        display: none;
                    }
                }
            </style>
        <?php }    echo '<li><a class="header__nav-link '.$class.'" title="'.$navItem['title'].'" href="'.$navItem['url'].'"><span>'.$navItem['title'].'</span></a></li>';
        
    }
    if($navItem['submenu']) {
        echo '<div class="header__nav-submenu"><ul>';
        foreach ($navItem['submenu'] as $submenu) {
            echo '<li><a class="header__nav-link" title="'.$submenu['title'].'" href="'.$submenu['url'].'">'.$submenu['title'].'</a></li>';
        }
        echo '</ul></div>';
    }
    if($navItem['submenu']) {
        echo '</li>';
    }
}
echo '</ul></nav>';
// <nav class="header__nav">
// <ul>
//     <li>
//         <a class="header__nav-link active" href="practice-area.html"><span>Practice Areas</span></a></li>
//     <li><a class="header__nav-link" href="Attorneys.html"><span>Attorneys</span></a></li>
//     <li> <a class="header__nav-link" href="/"><span>Client Results</span></a></li>
//     <li><a class="header__nav-link" href="perspectives.html"><span>Perspectives</span></a></li>
//     <li class="with-submenu"><a class="header__nav-link"
//             href="contact-us.html"><span>Contact</span></a></li>
// </ul>
// </nav>