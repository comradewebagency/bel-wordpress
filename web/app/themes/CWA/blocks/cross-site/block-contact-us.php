<?php
    $text_address_block = get_field("text_address","option");
    $contact_us_title_block = get_field('contact_us_title','option');
    $contact_us_link_block = get_field('contact_us_link','option');
    $contact_us_form_title_block = get_field('contact_us_form_title','option');
    $phone_number_block = get_field('phone_number', 'option');
    $phone_number_two_block = get_field('phone_number_two', 'option');
    $google_maps_link_block = get_field('google_maps_link', 'option');
    $email_block = get_field('email', 'option');
    $link_block = get_field('link', 'option');
    $google_maps_link_title = get_field('google_maps_link_title', 'option');
    $work_hours_block = get_field('work_hours', 'option');
    $home_form_id = get_field("home_form_id",'option');
    $block_payment = get_field('payment','option');
    $block_client_portal = get_field('client_portal','option');
?>

<section class="contact-us-block">
    <div class="container">
        <div class="contact-us-block__row">
            <div class="contact-us-block__col">
                <?php if(!empty($contact_us_title_block)) { ?>
                    <h2 class="h2"><?php echo $contact_us_title_block ?></h2>
                <?php }?>
                <address class="contact-us-block__address">
                    <picture>
                    <img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/pin.svg" alt="Pin">
                    </picture>
                    <p>
                        <?php if(!empty($text_address_block)) { ?>
                            <?php echo $text_address_block ?>
                        <?php } ?>
    
                        <a class="directions" href="<?php echo  $google_maps_link_block ?>" target='_blank?>'>
                        <?php if(!empty($google_maps_link_title)) { ?>
                            <?php echo  $google_maps_link_title ?>
                        <?php } ?></a>
                    </p>
                </address>
                <div class="contact-information"><a class="contact-information__item" href="tel:<?php echo number_phone($phone_number_block)?>">
                        <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/phone.svg" alt="Phone"></picture><span><?php echo $phone_number_block ?></span>
                    </a>
                    <span picture class="contact-information__item" >
                        <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/fax.svg" alt="Fax"></picture><span><?php echo $phone_number_two_block ?></span>
                        </span>
                    
                    <a class="contact-information__item" href="mailto:<?php echo $email_block ?> " target="_blank">
                        <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/mail.svg" alt="mail"></picture>
                        <span><?php echo $email_block ?></span>
                    </a>
                    <a class="contact-information__item" href="<?php echo $link_block['url']?>" target="_blank">
                        <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/in.svg" alt="In"></picture><span><?php echo $link_block['title']?></span>
                    </a>


                    <?php if(is_page_template('pages/contact-us.php')) { ?>
                            <div class="wrap-contact-information" >
                                <a class="contact-information__item" href="<?php echo $block_payment['url']?>" target="_blank"><picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/money.svg" alt="Payment"></picture><span><?php echo $block_payment['title']?></span></a>
                                <a class="contact-information__item" href="<?php echo $block_client_portal['url']?>" target="_blank"><picture><img  src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/user-gray.svg" alt="Сlient portal"></picture><span><?php echo $block_client_portal['title']?></span></a></div>
                                <div class="contact-information__item" style="color: #DADADA">
                                        <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/clock.svg" alt="Clock"></picture>
                                        <span> <b>Working Hours:</b>
                                            <span><?php echo $work_hours_block ?></span>
                                        </span>
                                </div>
                    <?php } else { ?>
                        <div class="contact-information__item" style="color: #DADADA">
                            <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/clock.svg" alt="Clock"></picture>
                            <span> <b>Working Hours:</b>
                                <span><?php echo $work_hours_block ?></span>
                            </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr>
            <div class="contact-us-block__col">
                <div class="title-form">
                    <?php if(!empty($contact_us_form_title_block)) { ?>
                        <p><?php echo $contact_us_form_title_block ?></p>
                    <?php } ?>
                </div>
                
                        <?php if ($home_form_id) :
                            echo do_shortcode('[contact-form-7 id="'.$home_form_id.'" html_class="form"]');
                        endif; ?>

            </div>
        </div>
    </div>
</section>
