<?php
    $infoblock_content = get_field('infoblock_content','option');
    $infoblock_link = get_field('infoblock_link','option');
    global  $info_block_class;
?>
<div class='info-block <?php echo $info_block_class ?>'>
    <div class="info-block__container">
        <div class="info-block__row">
        <div class="info-block__col">
                <?php if(!empty($infoblock_content)) { echo $infoblock_content; ?> <?php } ?>
        </div>
        <div class="info-block__col">
            <?php if(!empty($infoblock_link)) { ?>
                <a class="btn btn--tilda" href="<?php echo $infoblock_link['url']?>" target="<?php echo $infoblock_link['target']?>"><span><?php echo $infoblock_link['title']?></span></a></div>
            <?php } ?>
        </div>
    </div>
</div>
