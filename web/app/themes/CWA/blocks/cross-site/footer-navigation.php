<?php

$menuLocations = get_nav_menu_locations();
$menuID = $menuLocations['footer_menu'];
$primaryNav = wp_get_menu_array($menuID);
$url = $_SERVER['REQUEST_URI'];
$url_absolute = home_url() . $url;
$i = 1;
echo '<ul class="footer-menu">';
foreach ($primaryNav as $navItem) {
    if ($url == $navItem['url'] || $url_absolute == $navItem['url']) {
        $class = 'active';
    } else {
        $class = '';
    }
    if ($navItem['submenu'] != false) {

        if ($navItem['title'] != '#') {
            echo '<div class="footer__list-title">';
            if (!$navItem['url'] || $navItem['url'] == '#') {
                echo '<span title="' . $navItem['title'] . '">' . $navItem['title'] . '</span>';
            } else {
                echo '<span  class="' . $class . '">' . $navItem['title'] . '</span>';
            }
        }
    } else {

        echo '<li><a class="'.$class.'" href='.$navItem["url"].'>' . $navItem['title'] . '</a></li>';

    }

    if ($navItem['submenu']) {
        echo '<ul class="footer__list ' . $no_title . '">';
        foreach ($navItem['submenu'] as $submenu) {
            echo '<li><a title="' . $submenu['title'] . '" href="' . $submenu['url'] . '">' . $submenu['title'] . '</a></li>';
        }
        echo '</ul>';
    }
    $i++;
}
