<?php
    $home_hero_slide_image_block = get_field('home_hero_slide_image');
    $perspectives_inner_block = get_field("perspectives_inner","option");
    $title_search = get_field('title_search','option');
    $default_image = get_field('default_image','option');
    if( $perspectives_inner_block ) { $background =  $perspectives_inner_block ; } else { $background = $default_image; }

?>
<?php if (is_404()) { ?>
    <section class="hero hero-inner hero-inner--perspectives hero-js">
        <?php if(!empty($background)) { ?>
            <picture class="hero-inner__bg">
                <img src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
            </picture>
        <?php } ?>
        <div class="container">
            <div class="hero-inner__row">
            <!-- <?php if(!empty($title_search)) { ?> -->
                <h1 class="h1 h2--white">404</h1>
            <!-- <?php }?> -->
            </div>
        </div>
</section>
<?php } elseif (is_search()) { ?>
<section class="hero hero-inner hero-inner--perspectives hero-js">
        <?php if(!empty($background)) { ?>
            <picture class="hero-inner__bg">
                <img src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
            </picture>
        <?php } ?>
        <div class="container">
            <div class="hero-inner__row">
            <?php if(!empty($title_search)) { ?>
                <h1 class="h1 h2--white"><?php echo $title_search?></h1>
            <?php }?>
            </div>
        </div>
</section>
<?php the_breadcrumb(); ?>
<?php } elseif (is_page() && !is_page_template()) { ?>
    <section class="hero hero-inner hero-inner--perspectives hero-js">
        <?php if(!empty($background)) { ?>
            <picture class="hero-inner__bg">
                <img src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
            </picture>
        <?php } ?>
        <div class="container">
            <div class="hero-inner__row">
                <h1 class="h1 h2--white"><?php echo get_the_title()?></h1>
            </div>
        </div>
    </section>
<?php the_breadcrumb(); ?>
<?php } elseif (is_singular('practice')) { ?>
    <section class="hero hero-inner hero-js">
        <?php if(!empty($background)) { ?>
            <picture class="hero-inner__bg">
                <img src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
            </picture>
        <?php } ?>
        <div class="container">
            <div class="hero-inner__row">
                <h1 class="h1 h2--white"><?php echo get_the_title()?></h1>
            </div>
        </div>
    </section>
<?php the_breadcrumb(); ?>
<?php } else {?>
        <section class="hero hero-inner hero-inner--perspectives  hero-js">
            <?php if(!empty($background)) { ?>
                <picture class="hero-inner__bg">
                    <img data-src="<?php echo $background['url']?>" alt="<?php echo $background['alt']?>">
                </picture>
            <?php } ?>
            <div class="container">
            <div class="hero-inner__row">
                <div class="content-cards">
                        <span class="content-cards__item-date"><?php echo get_the_time('M j, Y') ?></span>
                </div>
                <?php if(!empty( is_singular('perspectives'))) { ?>
                    <h1 class="h2 h2--white"><?php echo get_the_title(); ?></h1>
                <?php } ?>
            </div>
            </div>
    </section>
    <?php the_breadcrumb(); ?>
<?php } ?>
