<?php
    $icon_block =  get_field("social_networks" ,"option");
    $top_content =  get_field("text_top");
    $bottom_content =  get_field("text_bottom");
    $link_top = get_field("link_top");
    $link_bottom = get_field("link_bottom");
    $more_on_block = get_field('more_on');
    $args = array(
        'post_type' => 'perspectives',
        'posts_per_page' => 9,
        'order' => 'DESC',
        'post__not_in' => array(get_the_ID())
    );
    $query = new WP_Query($args);
?>
<section class="perspectives-inner">
    <div class="container">
          <div class="perspectives-inner__row">
                <div class="perspectives-inner__col">
                    <div class="social">
                        
                        <a   class="social__item"  href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" 
                            data-template="https://www.facebook.com/sharer/sharer.php?u=_LINK_"
                        target="_blank"><i class="fab fa-facebook-f"></i>
                        </a>

                        <a  class="social__item" href="https://twitter.com/intent/tweet?text=<?php echo get_the_title() . ' / '; ?><?php echo get_permalink(); ?>" 
                            data-template="https://twitter.com/intent/tweet?text=_TITLE_ / _LINK_"
                        target="_blank"><i class="fab fa-twitter"></i></a>

                        <a class="social__item" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&source=LinkedIn"
                            data-template="https://www.linkedin.com/shareArticle?mini=true&url=_LINK_&title=_TITLE_&source=LinkedIn"
                        target="_blank"><i class="fab fa-linkedin"></i></a>
                        
                        <a class="social__item" target="_blank" href="mailto:?subject=<?php echo get_the_title(); ?>&amp;body=<?php echo get_permalink(); ?>"
                            data-template="mailto:?subject=_TITLE_&body=_LINK_"
                        title="<?php echo get_the_title(); ?>">
                            <i class="fal fa-envelope"></i>
                        </a>
                        
                    </div>
                </div>
               
                <div class="perspectives-inner__col">
                    <div class="perspectives-inner__content">
                        <?php if(!empty( $top_content )) { ?>
                            <?php echo  $top_content ?>
                        <?php } ?>

                        <?php if(!empty($link_top)) { ?>
                            <div class="perspectives-inner__description"><b>See also:<a href="<?php echo $link_top['url']?>" target="<?php echo$link_top['target']?>"><?php echo$link_top['title']?></a></b></div>
                        <?php }  ?>
                    </div>
                    
                    <?php if($more_on_block == 1 ) { ?>
                        <div class="clearfix"></div>
                        <?php get_template_part('blocks/cross-site/block', 'infoblock')?>
                    <?php } ?>
                   
                    <div class="perspectives-inner__content">
                       <?php if(!empty($bottom_content)) { ?>
                            <?php echo $bottom_content?>
                       <?php } ?>
                    </div>
                    <?php if(!empty($link_bottom)) { ?>
                            <div class="perspectives-inner__description"><b>See also:<a href="<?php echo $link_bottom['url']?>" target="<?php echo$link_bottom['target']?>"><?php echo$link_bottom['title']?></a></b></div>
                    <?php } ?>
                </div>
        </div>
    </div>
</section>
