<?php
$args = array(
        "order"=>'desc',
        'post_type' => 'perspectives',
        'post_status' => 'publish',
        'posts_per_page' => 2,
        'post__not_in' => array(get_the_ID()),
        'orderby' => 'date',
);
$query = new WP_Query( $args );

$navigation_title_block = get_field('navigation_title','option');
$navigation_link_block = get_field('navigation_link','option');
$default_image = get_field('default_image','option');
?>

<div class="content-cards content-cards--blog-preview">
    <div class="container">
        <div class="content-cards__row content-cards__row--title">
            <?php if(!empty($navigation_title_block)) { ?>
            <h2> <?php echo $navigation_title_block?></h2>
            <?php } ?>
            <?php if(!empty($navigation_link_block)) { ?>
            <a class="link link__icon uppercase" href="<?php echo $navigation_link_block['url']?>"
                target="<?php echo $navigation_link_block['target']?>">
                <picture><img src="<?php echo get_template_directory_uri(); ?>/front/dist/assets/img/arrow-small.svg"
                        alt="Icon"></picture><span><?php echo $navigation_link_block['title']?></span>
            </a>

            <?php } ?>
        </div>
        <hr style="background: #F0F0F0; height: 2px; width: 100%; margin-bottom: 40px; display: block; border: none; opacity: 1;">
        <div class="content-cards__row">
            <?php while ($query->have_posts()) : $query->the_post();?>
            <?php $text_top_block = get_field("text_top"); ?>
                <div class="content-cards__item " >
                <?php if (!empty(get_the_post_thumbnail_url())) {?>
                    <a href="<?php echo get_permalink( );?>" rel="nofollow">
                        <picture class="content-cards__img">
                            <img data-src="<?php echo  get_the_post_thumbnail_url( )?>">
                        </picture>
                    </a>
                <?php }  ?>

                <span class="content-cards__item-text">

                    <a href="<?php echo get_permalink(  );?>" class="content-cards__item-title"><?php echo get_the_title(); ?></a>
                    <span
                        class="content-cards__item-subtitle"><?php echo  wp_trim_words($text_top_block, 30, "...");?></span>

                    <span class="content-cards__item-date"><?php echo get_the_time('M j, Y') ?></span>
                </span>
                <span class="content-cards__arrow"></span>



            </div>
        <?php   endwhile; wp_reset_query(); ?>
        </div>
    </div>
</div>
