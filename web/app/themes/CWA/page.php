<?php
get_header();

?>
<style>
    
    .template {
        /* margin-top: 70px; */
        padding-bottom: 70px;
    }
    /* h2,h3,h1 {
        max-width : 700px;
    } */
</style>

    <?php echo get_template_part('blocks/cross-site/block', 'hero')?>
    <section class="template">
        <div class="container visual-editor">
            <?php
            the_post();
            the_content();
            ?>
        </div>
    </section>
<?php
get_footer();