<?php
$info_block_class = 'info-block--blue'; 
get_header();
    get_template_part('blocks/cross-site/block', 'hero');
    get_template_part('blocks/perspective-inner/perspectives', 'inner');
    get_template_part('blocks/perspective-inner/block', 'navigation');

    get_template_part('blocks/cross-site/block', 'contact-us');
get_footer();